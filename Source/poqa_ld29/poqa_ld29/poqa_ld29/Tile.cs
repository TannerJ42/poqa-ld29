﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace poqa_ld29
{
    class Tile
    {
        public bool Covered = false;
        Sprite sprite;
        Sprite transparency;
        Sprite sparkle;
        Visibility visibility = Visibility.dark;

        public Vector2 Center
        {
            get { return new Vector2(sprite.Position.X + sprite.Width / 2, sprite.Position.Y + sprite.Height / 2); }
        }

        public Rectangle HitRectangle
        {
            get { return sprite.DrawRectangle; }
        }

        public Tile(GameManager game, BlockTypes blockType, Vector2 location)
        {
            string image;
            int frames = 1;
            location *= CONSTANTS.BLOCK_SIZE;

            switch (blockType)
            {
                case BlockTypes.GrayBackground:
                    image = "Tiles/Background_Rock";
                    break;
                case BlockTypes.GreenBackground:
                    image = "Tiles/Background_Mossy";
                    break;
                case BlockTypes.YellowBackground:
                    image = "Tiles/Background_Dirt";
                    break;
                case BlockTypes.RedBackground:
                    image = "Tiles/Background_Lava";
                    break;
                case BlockTypes.OrangeBackground:
                    image = "Tiles/Background_Mineral";
                    break;
                case BlockTypes.BlackBackground:
                    image = "Tiles/Background_Obsidian";
                    break;
                default:
                    Console.WriteLine("Couldn't find case for BlockTypes " + blockType + " in constructor for Tile.");
                    image = "testBLock";
                    break;
            }

            sprite = new Sprite(game.Content.Load<Texture2D>(image), location);
            sprite.SetNumberOfFrames(frames);

            transparency = new Sprite(game.Content.Load<Texture2D>("Tiles/transparency"), location);
            transparency.LayerDepth = 0.9f;
            transparency.Width = CONSTANTS.BLOCK_SIZE;
            transparency.Height = CONSTANTS.BLOCK_SIZE;

            float[] rotationChoices = new float[4]
            {
                0f,
                (float)Math.PI,
                (float)(-Math.PI / 2),
                (float)(Math.PI / 2),
            };

            sprite.Rotation = rotationChoices[game.Seed.Next(rotationChoices.Length)];

            sprite.LayerDepth = 0.05f;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);

            Color color;

            switch (visibility)
            {
                case Visibility.light:
                    color = new Color(0, 0, 0, 0);
                    break;
                case Visibility.gray:
                    color = new Color(0, 0, 0, 100);
                    break;
                case Visibility.dark:
                    color = new Color(0, 0, 0, 255);
                    break;
                default:
                    color = new Color(0, 0, 0, 0);
                    break;
            }
            transparency.Color = color;

            transparency.Draw(spriteBatch);

            if (visibility == Visibility.light)
                visibility = Visibility.gray;
        }

        internal void Move(Vector2 direction)
        {
            sprite.Position += direction;
            transparency.Position += direction;
        }

        enum Visibility
        {
            dark,
            gray,
            light
        }

        internal void Uncover()
        {
            visibility = Visibility.light;
        }
    }
}
