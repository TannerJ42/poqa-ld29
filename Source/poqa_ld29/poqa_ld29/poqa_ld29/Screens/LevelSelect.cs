﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class ZoneSelect : Screen
    {
        Sprite label;
        Sprite foreground;

        List<TextSprite> Scores;
        List<TextSprite> modes;
        List<Button> leftButtons;
        List<Button> rightButtons;

        public List<int> HighScores;

        List<Button> currentButtonsList;

        Button currentButton;
        int currentButtonNumber;

        SpriteFont highScoreFont;

        public ZoneSelect(GameManager game)
            : base(game)
        {
            leftButtons = new List<Button>();
            rightButtons = new List<Button>();

            currentButtonsList = leftButtons;

            Scores = new List<TextSprite>();
            modes = new List<TextSprite>();

            label = new Sprite(game.Content.Load<Texture2D>("UI/Level_Select"), new Vector2(0, 0));
            foreground = new Sprite(game.Content.Load<Texture2D>("UI/Title_Screen_Front"), new Vector2(0, 0));

            leftButtons.Add(new Button(game, new Vector2(325, 190), "Beginner"));
            leftButtons.Add(new Button(game, new Vector2(325, 300), "Intermediate"));
            leftButtons.Add(new Button(game, new Vector2(325, 410), "Advanced"));
            leftButtons.Add(new Button(game, new Vector2(325, 520), "Expert"));

            foreach (Button b in leftButtons)
                b.SetColor(new Color(0, 0, 0, 0));

            HighScores = new List<int>()
            {
                0,
                0,
                0,
                0
            };


            rightButtons.Add(new Button(game, new Vector2(1000, 75), "Back"));
            rightButtons[0].ChangeWidth(0.5f);

            highScoreFont = game.Content.Load<SpriteFont>("buttonText");
            Scores.Add(new TextSprite(highScoreFont, "High Score: " + 0, new Vector2(645, 215)));
            Scores.Add(new TextSprite(highScoreFont, "High Score: " + 0, new Vector2(645, 325)));
            Scores.Add(new TextSprite(highScoreFont, "High Score: " + 0, new Vector2(645, 435)));
            Scores.Add(new TextSprite(highScoreFont, "High Score: " + 0, new Vector2(645, 545)));

            modes.Add(new TextSprite(highScoreFont, "Beginner", new Vector2(475, 215)));
            modes.Add(new TextSprite(highScoreFont, "Intermediate", new Vector2(475, 325)));
            modes.Add(new TextSprite(highScoreFont, "Advanced", new Vector2(475, 435)));
            modes.Add(new TextSprite(highScoreFont, "Expert", new Vector2(475, 545)));

            foreach (TextSprite t in modes)
            {
                t.Center();
                t.Position.Y -= 10;
            }
            foreach (TextSprite t in Scores)
            {
                t.Position.Y -= 10;
            }
        }

        void HighlightButton(Button button)
        {
            button.Highlight();
        }

        public override void Enter()
        {
            Console.WriteLine("Entering");
            player = game.player;

            currentButton = leftButtons[currentButtonNumber];

            currentButtonsList = leftButtons;

            HighlightButton(currentButton);

            game.SetHighScores();

            Scores[0].Text = "High Score: " + HighScores[0];
            Scores[1].Text = "High Score: " + HighScores[1];
            Scores[2].Text = "High Score: " + HighScores[2];
            Scores[3].Text = "High Score: " + HighScores[3];
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);


            if (player.PressedAction2)
            {
                game.ChangeScreen("MM");
                game.PlayCue("UI_Cancel");
            }

            if (player.PressedAction1)
            {
                

                if (currentButtonsList == leftButtons)
                {
                    game.PlayCue("UI_Choose");
                    string name = GetLevelName(currentButton.text);

                    int score = 0;

                    int number = 0;

                    if (name == "Levels/Level_1")
                        number = 0;
                    if (name == "Levels/Level_2")
                        number = 1;
                    if (name == "Levels/Level_3")
                        number = 2;
                    if (name == "Levels/Level_4")
                        number = 3;

                    score = HighScores[number];

                    game.EnterGameplay(name, number, score);
                }

                else
                {
                    currentButton.UnHighlight();
                    game.ChangeScreen("MM");
                    game.PlayCue("UI_Cancel");
                }
            }

            if (player.PressedDown)
            {
                ChangeButton(1);
            }
            else if (player.PressedUp)
            {
                ChangeButton(-1);
            }

            if (player.PressedRight)
            {
                if (currentButtonsList != rightButtons)
                {
                    game.PlayCue("UI_Select");
                    currentButtonsList = rightButtons;
                }
                ChangeButton(0);
            }
            else if (player.PressedLeft)
            {
                if (currentButtonsList != leftButtons)
                {
                    game.PlayCue("UI_Select");
                    currentButtonsList = leftButtons;
                }
                ChangeButton(0);
            }

            foreach (Button b in leftButtons)
            {
                b.Update(gameTime);
            }

            foreach (Button b in rightButtons)
            {
                b.Update(gameTime);
            }


        }

        private string GetLevelName(string name)
        {
            if (name == "Beginner")
                return "Levels/Level_1";
            if (name == "Intermediate")
                return "Levels/Level_2";
            if (name == "Advanced")
                return "Levels/Level_3";
            if (name == "Expert")
                return "Levels/Level_4";
            else
                return "";
        }

        private void ChangeButton(int direction)
        {
            

            if (currentButtonsList == leftButtons)
            {
                currentButton.UnHighlight();

                currentButtonNumber += direction;

                if (currentButtonNumber < 0)
                {
                    currentButtonNumber = 0;
                }
                else if (currentButtonNumber > leftButtons.Count - 1)
                {
                    currentButtonNumber = leftButtons.Count - 1;
                }

                if (currentButton != leftButtons[currentButtonNumber])
                {
                    game.PlayCue("UI_Select");
                    currentButton = leftButtons[currentButtonNumber];
                }
                currentButton.Highlight();
            }
            else
            {
                currentButton.UnHighlight();
                currentButton = rightButtons[0];
                currentButton.Highlight();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            base.Draw(spriteBatch);

            game.GraphicsDevice.Clear(Color.ForestGreen);
            label.Draw(spriteBatch);

            foreach (Button b in leftButtons)
            {
                b.Draw(spriteBatch);
            }

            foreach (Button b in rightButtons)
            {
                b.Draw(spriteBatch);
            }

            foreach (TextSprite t in Scores)
            {
                t.Draw(spriteBatch);

            }

            foreach (TextSprite t in modes)
            {
                t.Draw(spriteBatch);
            }

            foreground.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
