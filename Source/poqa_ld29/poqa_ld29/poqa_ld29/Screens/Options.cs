﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class Options : Screen
    {
        Sprite background;
        Sprite foreGround;

        public Options(GameManager game)
            : base(game)
        {
            background = new Sprite(game.Content.Load<Texture2D>("UI/Title_Screen_Back"), new Vector2(0, 0));
            foreGround = new Sprite(game.Content.Load<Texture2D>("UI/Title_Screen_Front"), new Vector2(0, 0));
        }

        public override void Enter()
        {
            Console.WriteLine("Entering");
            player = game.player;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedAction1)
            {
                game.ChangeScreen("ZS");
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            base.Draw(spriteBatch);



            game.GraphicsDevice.Clear(Color.ForestGreen);
            background.Draw(spriteBatch);

            foreGround.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
