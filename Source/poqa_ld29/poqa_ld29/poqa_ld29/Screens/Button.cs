﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class Button
    {
        public Player player;
        GameManager game;
        Sprite buttonSprite;
        ButtonState state;
        public string text;
        SpriteFont font;
        Vector2 textPosition;
        Color color = new Color(220, 221, 222, 255);

        public Button(GameManager game, Vector2 position, string text)
        {
            this.game = game;
            this.text = text;
            player = game.player;
            buttonSprite = new Sprite(game.Content.Load<Texture2D>(@"UI/Button"), position);
            buttonSprite.SetNumberOfFrames(3);

            textPosition = new Vector2(buttonSprite.Position.X + buttonSprite.Width / 2,
                                       buttonSprite.Position.Y + buttonSprite.Height / 4 - 10);

            font = game.Content.Load<SpriteFont>("buttonText");

            textPosition.X -= font.MeasureString(text).Length() / 2;

        }

        public void Update(GameTime gameTime)
        {
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            buttonSprite.Draw(spriteBatch);
            spriteBatch.DrawString(font, text, textPosition, color);
        }

        public void Highlight()
        {
            buttonSprite.SetFrame(2);
        }

        internal void UnHighlight()
        {
            buttonSprite.SetFrame(1);
        }

        internal void SetColor(Color color)
        {
            this.color = color;
        }

        public void ChangeWidth(float multiplier)
        {
            int pixels = buttonSprite.Width - (int)(multiplier * buttonSprite.Width);

            buttonSprite.Width = (int)(multiplier * buttonSprite.Width);

            textPosition.X -= pixels / 5;
            
        }
    }

    enum ButtonState
    {
        Released,
        Pressed,
        Highlighted
    }
}
