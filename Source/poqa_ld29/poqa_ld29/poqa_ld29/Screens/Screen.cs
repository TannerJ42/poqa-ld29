﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class Screen
    {
        protected GameManager game;
        protected Player player;

        public Screen(GameManager game)
        {
            this.game = game;
            this.player = game.player;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }

        public virtual void Enter()
        {

        }

        public virtual void Leave()
        {

        }
    }
}
