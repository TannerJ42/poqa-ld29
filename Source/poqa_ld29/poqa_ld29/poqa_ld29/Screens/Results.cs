﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class ResultsScreen : Screen
    {
        Sprite background;
        Sprite foreGround;

        List<Result> results;

        Sprite Tier0BlockIcon;
        Sprite Tier1BlockIcon;
        Sprite Tier2BlockIcon;
        Sprite Tier3BlockIcon;
        Sprite Tier4BlockIcon;
        Sprite Tier5BlockIcon;

        Sprite Tier3GemIcon;

        Sprite HighScoreImage1;
        Sprite HighScoreImage2;

        float timer = 3f;

        Animation highScoreAnimation;

        public ResultsScreen(GameManager game)
            : base(game)
        {
            background = new Sprite(game.Content.Load<Texture2D>("UI/Results"), new Vector2(0, 0));
            foreGround = new Sprite(game.Content.Load<Texture2D>("UI/Title_Screen_Front"), new Vector2(0, 0));

            
            //textSprites = new List<TextSprite>();

            int x = 450;
            int y = 205;
            int yOffset = 35;

            Tier0BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Rock"), new Vector2(x, y + yOffset * 0));
            Tier1BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Mossy"), new Vector2(x, y + yOffset * 1));
            Tier2BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Dirt"), new Vector2(x, y + yOffset * 2));
            Tier3BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Mineral"), new Vector2(x, y + yOffset * 3));
            Tier4BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Lava"), new Vector2(x, y + yOffset * 4));
            Tier5BlockIcon = new Sprite(game.Content.Load<Texture2D>("Tiles/Tile_Obsidian"), new Vector2(x, y + yOffset * 5));

            Tier3GemIcon = new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Diamond_Clear"), new Vector2(x, y+15+yOffset * 6));

            Tier3GemIcon.Scale(0.5f);

            HighScoreImage1 = new Sprite(game.Content.Load<Texture2D>("UI/NewHighScore"), new Vector2(50, 110));
            HighScoreImage1.Visible = false;

            HighScoreImage1.SetNumberOfFrames(4);
            HighScoreImage1.Scale(0.5f);

            HighScoreImage2 = new Sprite(game.Content.Load<Texture2D>("UI/NewHighScore"), new Vector2(1230, 90));
            HighScoreImage2.Visible = false;

            HighScoreImage2.SetNumberOfFrames(4);
            HighScoreImage2.Scale(0.5f);

            HighScoreImage2.Rotation = (float)(Math.PI / 5);

            HighScoreImage2.Position.X -= 3 * (HighScoreImage2.Width / 2);

            List<Sprite> blocks = new List<Sprite>()
                {
                    Tier0BlockIcon,
                    Tier1BlockIcon,
                    Tier2BlockIcon,
                    Tier3BlockIcon,
                    Tier4BlockIcon,
                    Tier5BlockIcon,
                };

            highScoreAnimation = new Animation(new int[4] { 1, 2, 3, 4 }, 0.25f, true);

            foreach (Sprite s in blocks)
            {
                s.SetNumberOfFrames(4);
                s.Scale(0.5f);
            }
        }

        public override void Enter()
        {
            player = game.player;

            timer = 3f;

            game.creature.AddTimerToScore();

            if (!game.creature.Alive)
            {
                game.creature.Timer = 0;
            }

            if (game.creature.Score > game.CurrentZone.HighScore)
                game.CurrentZone.HighScore = game.creature.Score;


            game.SubmitHighScore(game.creature.Score);

            GenerateScores(game.creature);
            

            if (game.creature.Score >= game.CurrentZone.HighScore)
            {
                HighScoreImage1.Visible = true;
                HighScoreImage2.Visible = true;
            }
            else
            {
                HighScoreImage1.Visible = false;
                HighScoreImage2.Visible = false;
            }

            game.SaveHighScores();
        }

        public override void Leave()
        {
            base.Leave();
            game.PlayBGM("Main_Title");
        }

        private void GenerateScores(Creature creature)
        {
            results = new List<Result>();

            SpriteFont font = game.Content.Load<SpriteFont>("HighScoreFont");
            SpriteFont largeFont = game.Content.Load<SpriteFont>("resultsFont");

            results.Add(new Result(Tier0BlockIcon, Tier0BlockIcon.Position, font, creature.BlocksBroken[0] * 50, "Blocks Crushed: " + ((int)(creature.BlocksBroken[0])).ToString() + " x 50  = "));
            results.Add(new Result(Tier1BlockIcon, Tier1BlockIcon.Position, font, creature.BlocksBroken[1] * 100, "Blocks Crushed: " + ((int)(creature.BlocksBroken[1])).ToString() + " x 100 = "));
            results.Add(new Result(Tier2BlockIcon, Tier2BlockIcon.Position, font, creature.BlocksBroken[2] * 150, "Blocks Crushed: " + ((int)(creature.BlocksBroken[2])).ToString() + " x 150 = "));
            results.Add(new Result(Tier3BlockIcon, Tier3BlockIcon.Position, font, creature.BlocksBroken[3] * 200, "Blocks Crushed: " + ((int)(creature.BlocksBroken[3])).ToString() + " x 200 = "));
            results.Add(new Result(Tier4BlockIcon, Tier4BlockIcon.Position, font, creature.BlocksBroken[4] * 250, "Blocks Crushed: " + ((int)(creature.BlocksBroken[4])).ToString() + " x 250 = "));
            results.Add(new Result(Tier5BlockIcon, Tier5BlockIcon.Position, font, creature.BlocksBroken[5] * 300, "Blocks Crushed: " + ((int)(creature.BlocksBroken[5])).ToString() + " x 300 = "));
            results.Add(new Result(Tier3GemIcon, Tier3GemIcon.Position, font, creature.DiamondsCollected * 1000, "Collected: " + ((int)(creature.DiamondsCollected)).ToString() + " x 1000 = "));
            results.Add(new Result(null, new Vector2(450, 473), font, (int)creature.Timer * 10, "Time Remaining: " + ((int)(creature.Timer)).ToString() + " x 10 = "));
            results.Add(new Result(null, new Vector2(640, 525), largeFont, (int)creature.Score, "Score: "));
            results.Add(new Result(null, new Vector2(640, 575), largeFont, (int)creature.HighScore, "High Score: "));
            results[8].Center();
            results[9].Center();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (timer > 0)
                timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (player.PressedAction1 &&
                timer <= 0)
            {
                game.ChangeScreen("ZS");
            }

            GenerateScores(game.creature);

            highScoreAnimation.Update(gameTime);

            HighScoreImage1.SetFrame(highScoreAnimation.frames[highScoreAnimation.CurrentFrame]);
            HighScoreImage2.SetFrame(highScoreAnimation.frames[highScoreAnimation.CurrentFrame]);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            base.Draw(spriteBatch);

            game.GraphicsDevice.Clear(Color.ForestGreen);
            background.Draw(spriteBatch);

            foreach (Result r in results)
                r.Draw(spriteBatch);

            HighScoreImage1.Draw(spriteBatch);
            HighScoreImage2.Draw(spriteBatch);

            foreGround.Draw(spriteBatch);

            spriteBatch.End();
        }
    }
}
