﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class MainMenuScreen : Screen
    {
        Sprite background;
        Sprite foreGround;

        int currentButtonNumber = 0;

        Button currentButton;

        List<Button> buttons;

        public MainMenuScreen(GameManager game)
            : base(game)
        {
            background = new Sprite(game.Content.Load<Texture2D>("UI/Main_Menu"), new Vector2(0, 0));
            foreGround = new Sprite(game.Content.Load<Texture2D>("UI/Title_Screen_Front"), new Vector2(0, 0));

            buttons = new List<Button>();

            buttons.Add(new Button(game, new Vector2(500, 300), "Levels"));
            buttons.Add(new Button(game, new Vector2(500, 440), "Credits"));

            

            buttons[0].Highlight();
        }

        public override void Enter()
        {
            Console.WriteLine("Entering");
            player = game.player;

            currentButton = buttons[currentButtonNumber];

            HighlightButton(currentButton);
        }

        void HighlightButton(Button button)
        {
            button.Highlight();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedAction1)
            {
                game.PlayCue("UI_Choose");

                string name = currentButton.text;

                if (name == "Credits")
                    game.ChangeScreen("Credits");
                else
                    game.ChangeScreen("ZS");
            }

            if (player.PressedDown)
            {
                ChangeButton(1);
            }
            else if (player.PressedUp)
            {
                ChangeButton(-1);
            }

            foreach (Button b in buttons)
            {
                b.Update(gameTime);
            }

            foreach (Button b in buttons)
            {
                b.Update(gameTime);
            }
        }

        private void ChangeButton(int direction)
        {
            

            currentButton.UnHighlight();

            currentButtonNumber += direction;

            if (currentButtonNumber < 0)
            {
                currentButtonNumber = 0;
            }
            else if (currentButtonNumber > buttons.Count - 1)
            {
                currentButtonNumber = buttons.Count - 1;
            }
            else
                game.PlayCue("UI_Select");

            currentButton = buttons[currentButtonNumber];

            currentButton.Highlight();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            base.Draw(spriteBatch);


            game.GraphicsDevice.Clear(Color.ForestGreen);
            background.Draw(spriteBatch);


            foreach (Button b in buttons)
                b.Draw(spriteBatch);

            foreGround.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
