﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class CreditsScreen : Screen
    {
        Sprite background;

        List<FloatingGem> gems;

        List<BlockTypes> types;

        public CreditsScreen(GameManager game)
            : base(game)
        {
            gems = new List<FloatingGem>();
            types = new List<BlockTypes>()
            {
                BlockTypes.PurpleGem,
                BlockTypes.OrangeGem,
                BlockTypes.GreenGem,
                BlockTypes.DiamondGem,
                BlockTypes.RedGem,
                BlockTypes.YellowGem,
                BlockTypes.BlueGem,
                BlockTypes.WhiteGem,
                BlockTypes.BlackGem
            };
            background = new Sprite(game.Content.Load<Texture2D>("UI/Credits"), new Vector2(0, 0));

            for (int i = game.Random.Next(5, 6); i >= 0; i--)
            {
                GenerateGem();
            }

            foreach (Gem g in gems)
            {
                g.sprite.Position.Y = game.Random.Next(0, 720);
            }
        }

        public override void Enter()
        {
            Console.WriteLine("Entering");
            player = game.player;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (player.PressedAction1)
            {
                game.ChangeScreen("MM");
                game.PlayCue("UI_Cancel");
            }

            if (player.PressedAction2)
            {
                game.ChangeScreen("MM");
                game.PlayCue("UI_Cancel");
            }

            for (int i = gems.Count - 1; i >= 0; i--)
            {
                gems[i].UpdateRotation(gameTime);
                if (gems[i].sprite.DrawRectangle.Top > 800)
                    gems.RemoveAt(i);
            }

            if (game.Random.Next(50) == 42)
            {
                GenerateGem();
            }

            if (game.Random.Next(50) == 42)
            {
                GenerateGem();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            base.Draw(spriteBatch);

            game.GraphicsDevice.Clear(Color.ForestGreen);
            background.Draw(spriteBatch);

            foreach (Gem g in gems)
                g.Draw(spriteBatch);

            spriteBatch.End();
        }

        void GenerateGem()
        {
            Vector2 position = new Vector2(game.Random.Next(-60, 1340), -60);

            BlockTypes blockType = types[game.Random.Next(types.Count)];

            gems.Add(new FloatingGem(game, blockType, position));
        }
    }
}
