﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29.Screens
{
    class Gameplay : Screen
    {
        Creature creature;

        Sprite HUD;

        Texture2D pixel;

        public bool FadingToBlack = false;
        public bool DoneFading = false;

        int fadeColor = 0;

        public Gameplay(GameManager game)
            : base(game)
        {
            pixel = game.Content.Load<Texture2D>("Tiles/transparency");
        }

        public override void Enter()
        {
            Console.WriteLine("Entering");
            creature = game.creature;
            player = game.player;

            HUD = new Sprite(game.Content.Load<Texture2D>("UI/HUD"), new Vector2());

            game.PlayBGM("BGM_Music");

            FadingToBlack = false;
            DoneFading = false;

            fadeColor = 0;

            creature.HighScore = game.CurrentZone.HighScore;
        }

        public override void Leave()
        {
            base.Leave();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (game.CurrentLevel != null)
                game.CurrentLevel.Update(gameTime);
            creature.Update(gameTime);

            foreach (Gem g in game.CurrentLevel.Pickups)
            {
                g.Update(gameTime);
            }

            //game.CurrentLevel.Exit.CheckForSparkle(game.CurrentLevel.Blocks);
            //game.CurrentLevel.Entrance.CheckForSparkle(game.CurrentLevel.Blocks);
            //foreach (Gem g in game.CurrentLevel.Pickups)
            //    g.CheckForSparkle(game.CurrentLevel.Blocks);

            game.CheckBorders();

            if (FadingToBlack)
            {
                fadeColor += 2;

                if (fadeColor > 255)
                {
                    fadeColor = 255;
                    DoneFading = true;
                    game.Lose();
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
            // blocks, entities only! using sprite sorting so define layer.
            spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            game.CurrentLevel.Draw(spriteBatch);
            creature.Draw(spriteBatch);
            spriteBatch.End();

            // UI, HUD
            spriteBatch.Begin();
            
            HUD.Draw(spriteBatch);

            creature.DrawUI(spriteBatch);

            game.CurrentLevel.DrawUI(spriteBatch);

            if (FadingToBlack)
            {
                spriteBatch.Draw(pixel, new Rectangle(0, 0, 1280, 720), new Color(0, 0, 0, fadeColor));
            }

            spriteBatch.End();
        }
    }
}
