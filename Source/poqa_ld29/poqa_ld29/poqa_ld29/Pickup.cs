﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Pickup
    {
        public Sprite sprite;

        public bool Active = true;

        public Pickup(GameManager game, BlockTypes blockType, Vector2 location)
        {
            location *= CONSTANTS.BLOCK_SIZE;

            string image = "blankBlock";

            sprite = new Sprite(game.Content.Load<Texture2D>(image), location);
            sprite.LayerDepth = 0.5f;
        }

        public virtual void Update(GameTime gameTime)
        {
            sprite.Update(gameTime);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }

        public virtual void Move(Vector2 direction)
        {
            sprite.Position += direction;
        }

        public virtual void Collect(Creature creature)
        {
            
        }

        public virtual void CheckForSparkle(List<Block> blocks)
        {

        }
    }
}
