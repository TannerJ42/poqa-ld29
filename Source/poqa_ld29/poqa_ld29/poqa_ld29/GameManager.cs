﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace poqa_ld29
{
    class GameManager
    {
        public GraphicsDevice GraphicsDevice;
        public ContentManager Content;
        public AudioManager audioManager;
        LevelLoader levelLoader;
        public Player player;
        public Creature creature;
        
        Rectangle gameplayArea;
        public Random Seed;
        public Random Random;
        public Zone CurrentZone;

        Screens.Screen CurrentScreen;
        Screens.TitleScreen TitleScreen;
        Screens.MainMenuScreen MainMenuScreen;
        Screens.CreditsScreen CreditsScreen;
        Screens.ResultsScreen ResultsScreen;
        Screens.Gameplay GameplayScreen;
        Screens.ZoneSelect ZoneSelectScreen;

        SaveManager saveManager;

        public Level CurrentLevel
        {
            get { return CurrentZone.Levels[CurrentZone.CurrentLevel]; }
        }

        public GameManager(GraphicsDevice graphics, ContentManager content)
        {
            Random = new Random();
            Seed = new Random(101010);

            player = new Player();

            this.GraphicsDevice = graphics;
            this.Content = content;
            audioManager = new AudioManager();
            levelLoader = new LevelLoader(this, content);

            GameplayScreen = new Screens.Gameplay(this);
            ZoneSelectScreen = new Screens.ZoneSelect(this);
            TitleScreen = new Screens.TitleScreen(this);
            MainMenuScreen = new Screens.MainMenuScreen(this);
            CreditsScreen = new Screens.CreditsScreen(this);
            ResultsScreen = new Screens.ResultsScreen(this);

            ChangeScreen("TS");

            gameplayArea = new Rectangle(300, 200, 800 - 500, 600 - 400);

            saveManager = new SaveManager();
        }

        public void SetHighScores()
        {
            SaveData data = saveManager.CreateOrLoadSave();

            ZoneSelectScreen.HighScores[0] = data.score0;
            ZoneSelectScreen.HighScores[1] = data.score1;
            ZoneSelectScreen.HighScores[2] = data.score2;
            ZoneSelectScreen.HighScores[3] = data.score3;
        }

        public void SaveHighScores()
        {
            SaveData data = new SaveData();
            data.score0 = ZoneSelectScreen.HighScores[0];
            data.score1 = ZoneSelectScreen.HighScores[1];
            data.score2 = ZoneSelectScreen.HighScores[2];
            data.score3 = ZoneSelectScreen.HighScores[3];

            saveManager.SaveData(data);
        }

        public void EnterGameplay(string zoneName, int number, int score)
        {
            creature = new Creature(this, player);
            LoadZone(zoneName, number, score);
            GameplayScreen.Enter();
            CurrentScreen = GameplayScreen;
            creature.SetPosition(CurrentLevel.Entrance.sprite.Position);
            creature.currentLevel = CurrentLevel;
        }

        public void LoadZone(string name, int number, int HighScore)
        {
            CurrentZone = new Zone(this, number);

            CurrentZone.HighScore = HighScore;

            levelLoader.LoadLevel(CurrentZone, Content.Load<Texture2D>(name + "01"));
            levelLoader.LoadLevel(CurrentZone, Content.Load<Texture2D>(name + "02"));
            levelLoader.LoadLevel(CurrentZone, Content.Load<Texture2D>(name + "03"));
            levelLoader.LoadLevel(CurrentZone, Content.Load<Texture2D>(name + "04"));
            levelLoader.LoadLevel(CurrentZone, Content.Load<Texture2D>(name + "05"));
            CurrentZone.Levels[0].DisableEntrance();

            Console.WriteLine("Set current level.");
        }

        public void CheckBorders()
        {
            if (creature.sprite.Visible)
            {
                if (creature.sprite.DrawRectangle.Right > gameplayArea.Right)
                {
                    float difference = creature.sprite.DrawRectangle.Right - gameplayArea.Right;

                    CurrentLevel.Move(new Vector2(-difference, 0));

                    creature.sprite.Position.X -= difference;
                }

                if (creature.sprite.DrawRectangle.Left < gameplayArea.Left)
                {
                    float difference = gameplayArea.Left - creature.sprite.DrawRectangle.Left;

                    CurrentLevel.Move(new Vector2(difference, 0));

                    creature.sprite.Position.X += difference;
                }

                if (creature.sprite.DrawRectangle.Top < gameplayArea.Top)
                {
                    float difference = gameplayArea.Top - creature.sprite.DrawRectangle.Top;

                    CurrentLevel.Move(new Vector2(0, difference));

                    creature.sprite.Position.Y += difference;
                }

                if (creature.sprite.DrawRectangle.Bottom > gameplayArea.Bottom)
                {
                    float difference = creature.sprite.DrawRectangle.Bottom - gameplayArea.Bottom;

                    CurrentLevel.Move(new Vector2(0, -difference));

                    creature.sprite.Position.Y -= difference;
                }
            }
        }

        public bool HasStorageDevice()
        {
            return saveManager.HasDevice;
        }

        public void UpdateStorageDevice()
        {
            saveManager.Update();
        }

        public void ChangeScreen(string name)
        {
            if (CurrentScreen != null)
                CurrentScreen.Leave();

            if (name == "TS")
            {
                CurrentScreen = TitleScreen;
            }
            else if (name == "MM")
            {
                CurrentScreen = MainMenuScreen;
            }
            else if (name == "ZS")
            {
                CurrentScreen = ZoneSelectScreen;
            }
            else if (name == "ZS")
            {
                CurrentScreen = ZoneSelectScreen;
            }
            else if (name == "Credits")
            {
                CurrentScreen = CreditsScreen;
            }
            else if (name == "Results")
            {
                CurrentScreen = ResultsScreen;
            }

            CurrentScreen.Enter();
        }

        internal void Leave(Doors door)
        {
            Console.WriteLine("Leaving!");

            creature.CanLeave = false;

            if (CurrentZone.LastLevel &&
                door == Doors.Exit)
            {
                Win();
            }
            else
            {
                if (door == Doors.Exit) // advancing
                {
                    CurrentZone.GoUpOneLevel();
                    LevelLoader.CenterLevel(CurrentLevel, Doors.Entrance);
                    creature.sprite.Position = CurrentLevel.Entrance.sprite.Position;
                    creature.sprite.Visible = true;
                    creature.currentLevel = CurrentLevel;
                }
                else // going back
                {
                    CurrentZone.GoDownOneLevel();
                    LevelLoader.CenterLevel(CurrentLevel, Doors.Exit);
                    creature.sprite.Position = CurrentLevel.Exit.sprite.Position;
                    creature.sprite.Visible = true;
                    creature.currentLevel = CurrentLevel;
                }
            }
        }

        internal void PlayCue(string name)
        {
            audioManager.PlayCue(name);
        }

        internal void Draw(SpriteBatch spriteBatch)
        {
            CurrentScreen.Draw(spriteBatch);
        }

        void Win()
        {
            ChangeScreen("Results");
            PlayBGM("Victory_Music");
        }

        public void Lose()
        {
            ChangeScreen("Results");
        }

        internal void Update(GameTime gameTime)
        {
            player.Update(gameTime);
            CurrentScreen.Update(gameTime);
        }

        internal void PlayBGM(string p)
        {
            audioManager.PlayBGM(p);
        }

        internal void StopBGM()
        {
            audioManager.StopBGM();
        }

        internal void StartLosing()
        {
            GameplayScreen.FadingToBlack = true;
        }

        public void StartBerserkAudio()
        {
            audioManager.StartBerserk();
        }

        public void StopBerserkAudio()
        {
            audioManager.StopBerserk();
        }

        internal void SubmitHighScore(int newScore)
        {
            int oldScore = CurrentZone.HighScore;

            int i = CurrentZone.Number;

            if (newScore >= oldScore)
            {
                ZoneSelectScreen.HighScores[i] = newScore;
            }
        }
    }
}
