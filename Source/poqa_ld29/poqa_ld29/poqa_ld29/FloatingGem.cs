﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class FloatingGem : Gem
    {
        public float rotationSpeed;

        public float fallSpeed;


        public FloatingGem(GameManager game, BlockTypes blockType, Vector2 position)
            : base(game, blockType, position)
        {
            rotationSpeed = game.Random.Next(0, 11);
            rotationSpeed -= 5;
            rotationSpeed *= 0.05f;

            fallSpeed = game.Random.Next(20, 50);
            fallSpeed *= 0.1f;
        }

        public void UpdateRotation(GameTime gameTime)
        {
            sprite.Rotation += rotationSpeed;
            Move(new Vector2(0, fallSpeed));
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
