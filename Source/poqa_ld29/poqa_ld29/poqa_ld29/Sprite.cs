﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace poqa_ld29
{

    class Sprite
    {
        Texture2D texture;
        public Vector2 Position;

        // lots of public variables - this is a game jam after all!
        public int Width;
        public int FrameWidth;
        public int Height;
        public float Rotation = 0;
        public Vector2 Origin = Vector2.Zero;
        public SpriteEffects SpriteEffect = SpriteEffects.None;
        public float LayerDepth = 0;
        public Color Color = Color.White;
        public bool Visible = true;
        public Rectangle SourceRectangle;

        /// <summary>
        /// Return a rectangle based on Position, Width, Height
        /// </summary>
        public Rectangle DrawRectangle
        {
            get { return new Rectangle((int)Position.X, (int)Position.Y, Width, Height); }
        }

        public Sprite(Texture2D texture, Vector2 position)
        {
            this.texture = texture;
            this.Position = position;

            Width = texture.Width;
            FrameWidth = Width;
            Height = texture.Height;

            SourceRectangle = new Rectangle(0, 0, FrameWidth, Height);
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            Rectangle rectangle = DrawRectangle;

            rectangle.X += rectangle.Width / 2;
            rectangle.Y += rectangle.Height / 2;

            Origin = new Vector2(rectangle.Width / 2, rectangle.Height / 2);

            if (Visible)
                spriteBatch.Draw(texture, rectangle, SourceRectangle, Color, Rotation, Origin, SpriteEffect, LayerDepth);
        }

        public virtual void DrawExplosion(SpriteBatch spriteBatch)
        {
            if (Visible)
                spriteBatch.Draw(texture, DrawRectangle, SourceRectangle, Color, Rotation, Origin, SpriteEffect, LayerDepth);
        }

        /// <summary>
        /// Center the sprite to its initial Vector2
        /// </summary>
        /// <param name="horizontal">"Center horizontally"</param>
        /// <param name="vertical">"Center vertically"</param>
        public void Center(bool horizontal, bool vertical)
        {
            if (horizontal)
                Position.X -= Width / 2;
            if (vertical)
                Position.Y -= Width / 2;
        }

        internal void SetNumberOfFrames(int frames)
        {
            Width = texture.Width / frames;
            SourceRectangle.Width = Width;
            FrameWidth = Width;
        }

        internal void SetFrame(int frame)
        {
            SourceRectangle.X = (frame - 1) * FrameWidth;
        }

        public Vector2 GetCenter
        {
            get { return new Vector2(Position.X + Width / 2, Position.Y + Height / 2); }
        }

        public void Scale(float scale)
        {
            Width = (int)(Width * scale);
            Height = (int)(Height * scale);
        }
    }
}
