﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace poqa_ld29
{
    class CONSTANTS
    {
        public static int SCREEN_WIDTH = 1280;
        public static int SCREEN_HEIGHT = 720;

        public static int BLOCK_SIZE = 60;
    }
}