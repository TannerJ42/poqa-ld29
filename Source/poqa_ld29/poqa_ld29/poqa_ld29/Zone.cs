﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace poqa_ld29
{
    class Zone
    {
        GameManager game;
        public int CurrentLevel = 0;
        public List<Level> Levels;

        public int HighScore;

        public float Timer = 200;
        public int Number;

        public Zone(GameManager game, int number)
        {
            Number = number;
            this.game = game;
            Levels = new List<Level>();

            Console.WriteLine(number);
        }

        public void Update(GameTime gameTime)
        {

        }

        internal void GoUpOneLevel()
        {
            Console.WriteLine("Going up a level.");
            if (++CurrentLevel > Levels.Count - 1)
                CurrentLevel = Levels.Count - 1;
        }

        internal void GoDownOneLevel()
        {
            if (--CurrentLevel < 0)
                CurrentLevel = 0;
        }

        public bool LastLevel
        {
            get { return (CurrentLevel == Levels.Count - 1); }
        }
    }
}
