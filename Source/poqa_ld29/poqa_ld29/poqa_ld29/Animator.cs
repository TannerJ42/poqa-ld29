﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Animator
    {
        Sprite sprite;
        Creature creature;

        public float Velocity;
        public bool Attacking = false;
        public bool Dying = false;

        Animation currentAnimation;
        Animation standingAnimation;
        Animation walkingAnimation;
        Animation punchingAnimation;
        Animation dyingAnimation;
        Animation berserkAnimation;

        public Animator(Creature creature, Sprite sprite)
        {
            this.creature = creature;
            this.sprite = sprite;
            standingAnimation = new Animation(new int[1] { 1 }, 1f, true);
            walkingAnimation = new Animation(new int[4] { 2, 3, 4, 5 }, 0.1f, true);
            punchingAnimation = new Animation(new int[8] { 6, 7, 8, 9, 10, 11, 12, 13 }, 0.01f, false);
            dyingAnimation = new Animation(new int[] { 14, 15, 16 }, 0.5f, false);

            berserkAnimation = new Animation(new int[4] { 1, 2, 3, 4 }, 0.25f, true);

            currentAnimation = standingAnimation;
        }

        public void Update(GameTime gameTime)
        {
            if (Dying)
            {
                Dying = false;
                //creature.StartDying();
                currentAnimation = dyingAnimation;
            }

            if (currentAnimation == punchingAnimation &&
                currentAnimation.Finished)
            {
                creature.StopAttacking();
            }
            if (currentAnimation == dyingAnimation &&
                currentAnimation.Finished)
            {
                creature.StopDying();
            }
            if (currentAnimation.Finished &&
                currentAnimation != dyingAnimation)
            {

                if (Attacking)
                {
                    Attacking = false;
                    punchingAnimation.Finished = false;
                    creature.StartAttacking();
                    currentAnimation = punchingAnimation;
                    punchingAnimation.CurrentFrame = 0;
                }
                else if (Velocity > 0)
                    currentAnimation = walkingAnimation;
                else
                    currentAnimation = standingAnimation;
            }
            currentAnimation.Update(gameTime);

            if (creature.Berserk)
            {
                berserkAnimation.Update(gameTime);
                creature.berserkSprite.SetFrame(berserkAnimation.frames[berserkAnimation.CurrentFrame]);
            }
            sprite.SetFrame(currentAnimation.frames[currentAnimation.CurrentFrame]);
        }
    }

    class Animation
    {
        public int CurrentFrame;
        public int[] frames;
        float frameRate;
        float timer;
        public bool Finished = true;
        bool loop;

        public Animation(int[] frames, float frameRate, bool loop)
        {
            this.loop = loop;
            if (!loop)
                Finished = false;
            this.frames = frames;
            this.frameRate = frameRate;

            CurrentFrame = 0;
        }

        public void Update(GameTime gameTime)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (timer >= frameRate) // time to change frames
            {
                timer = 0;

                CurrentFrame++;

                if (CurrentFrame > frames.Length - 1)
                {
                    if (loop)
                        CurrentFrame = 0;
                    else
                    {
                        CurrentFrame = frames.Length - 1;
                        Finished = true;
                    }
                }
            }
        }
    }
}
