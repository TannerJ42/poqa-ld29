﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Door
    {
        public bool Open = true;
        public Sprite sprite;
        public Sparkle sparkle;
        public bool Covered = false;

        Block block;

        public Rectangle Rectangle
        {
            get { return new Rectangle(sprite.DrawRectangle.X + sprite.DrawRectangle.Width / 4,
                                       sprite.DrawRectangle.Y + sprite.DrawRectangle.Height / 4,
                                       sprite.Width / 2,
                                       sprite.Height / 2);}
        }

        public Door(GameManager game, Doors type, Vector2 position)
        {
            string imageName;
            if (type == Doors.Entrance)
                imageName = @"Gems/Gem_Start";
            else
                imageName = @"Gems/Gem_End";

            sprite = new Sprite(game.Content.Load<Texture2D>(imageName), position);

            sprite.LayerDepth = 0.4f;

            sparkle = new Sparkle(game, sprite.Position);
            sparkle.sprite.LayerDepth = 0.9f;
        }

        public void Update(GameTime gameTime)
        {
            if (block != null &&
                !block.Solid)
            {
                Covered = false;
            }
            if (Covered)
                sparkle.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);

            if (Covered)
                sparkle.Draw(spriteBatch);
        }

        public void Move(Vector2 direction)
        {
            sprite.Position += direction;
        }

        public void CheckForSparkle(List<Block> blocks)
        {
            foreach (Block b in blocks)
                if (b.HitRectangle.Intersects(sprite.DrawRectangle))
                {
                    Covered = true;
                    block = b;
                }
        }
    }

    public enum Doors
    {
        None,
        Entrance,
        Exit
    }
}
