﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace poqa_ld29
{
    class Block
    {
        Sprite sprite;
        Sprite explosionSprite;

        Animation explosionAnimation;

        int MaxHP;
        int HP;
        public int Rank;
        int MaxFrames;

        GameManager game;

        public bool Explosive = false;
        public bool Exploding = false;
        float explosionTimer = 2f;


        public bool Breakable = false;
        public bool Solid = true;

        public Rectangle HitRectangle
        {
            get { return sprite.DrawRectangle; }
        }

        public Block(GameManager game, BlockTypes blockType, Vector2 location)
        {
            this.game = game;
            string image;
            int frames = 1;
            location *= CONSTANTS.BLOCK_SIZE;

            switch (blockType)
            {
                case BlockTypes.Blocker:
                    image = "Tiles/Tile_Adamantium";
                    Breakable = false;
                    break;
                case BlockTypes.FakeBlocker:
                    image = "Tiles/Tile_Adamantium";
                    Breakable = false;
                    Solid = false;
                    break;
                case BlockTypes.GrayBlock:
                    image = "Tiles/Tile_Rock";
                    Breakable = true;
                    Rank = 0;
                    frames = 4;
                    HP = 9;
                    break;
                case BlockTypes.GreenBlock: // mossy
                    image = "Tiles/Tile_Mossy";
                    Breakable = true;
                    Rank = 1;
                    frames = 4;
                    HP = 12;
                    break;
                case BlockTypes.YellowBlock:
                    image = "Tiles/Tile_Dirt";
                    Breakable = true;
                    Rank = 2;
                    frames = 4;
                    HP = 15;
                    break;
                case BlockTypes.OrangeBlock:
                    image = "Tiles/Tile_Mineral";
                    Breakable = true;
                    Rank = 3;
                    frames = 4;
                    HP = 19;
                    break;
                case BlockTypes.RedBlock:
                    image = "Tiles/Tile_Lava";
                    Breakable = true;
                    Rank = 4;
                    frames = 4;
                    HP = 30;
                    break;
                case BlockTypes.BlackBlock:
                    image = "Tiles/Tile_Obsidian";
                    Breakable = true;
                    Rank = 5;
                    frames = 4;
                    HP = 45;
                    break;
                case BlockTypes.ExplosiveBlock:
                    image = "Tiles/Tile_Bomb";
                    Breakable = true;
                    Explosive = true;
                    frames = 4;
                    Rank = 0;
                    HP = 1;
                    break;
                default:
                    Console.WriteLine("Couldn't find case for BlockTypes " + blockType + " in constructor for Block.");
                    image = "testBlock";
                    break;
            }

            MaxHP = HP;

            sprite = new Sprite(game.Content.Load<Texture2D>(image), location);
            sprite.SetNumberOfFrames(frames);

            float[] rotationChoices = new float[4]
            {
                0f,
                (float)Math.PI,
                (float)(-Math.PI / 2),
                (float)(Math.PI / 2),
            };

            sprite.Rotation = rotationChoices[game.Seed.Next(rotationChoices.Length)];

            MaxFrames = frames;
            sprite.LayerDepth = 0.7f;
           

            explosionSprite = new Sprite(game.Content.Load<Texture2D>("Tiles/Bomb_Countdown"), location);
            explosionSprite.SetNumberOfFrames(6);
            explosionSprite.Rotation = sprite.Rotation;
            explosionAnimation = new Animation(new int[6] { 1, 2, 3, 4, 5, 6 }, 1f / 3f, true);
            explosionSprite.LayerDepth = 0.7f;
        }

        public void Update(GameTime gameTime)
        {
            sprite.Update(gameTime);

            if (Exploding)
            {
                explosionTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                explosionAnimation.Update(gameTime);

                explosionSprite.SetFrame(explosionAnimation.frames[explosionAnimation.CurrentFrame]);

                if (explosionTimer <= 0)
                {
                    sprite.Visible = true;
                    FinishExploding();
                }
            }
        }

        private void FinishExploding()
        {
            sprite.SetFrame(4);
            sprite.LayerDepth = 0.1f;
            Solid = false;
            Exploding = false;
            Explode();
        }

        private void Explode()
        {
            game.CurrentLevel.AddExplosion(sprite.Position);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);

            if (Exploding)
                explosionSprite.Draw(spriteBatch);
        }

        public void TakeDamage(GameManager game, Creature creature, int BreakerRank, int damage)
        {
            if (Breakable &&
                Rank <= BreakerRank)
            {
                if (damage < 0)
                    damage = 0;
                HP -= damage;
                if (HP < 0)
                    HP = 0;
                CalculateFrame();
                if (HP <= 0)
                {
                    creature.BrokeBlock(Rank);
                    if (Explosive)
                        StartExploding();
                    else
                        Break(game);
                }

                if (damage > 0)
                {

                }
            }
        }

        private void StartExploding()
        {
            Breakable = false;
            Exploding = true;
            game.PlayCue("Bomb_Warning");
        }

        void CalculateFrame()
        {
            int frame = 1;

            if (!Explosive)
            {
                if (HP <= 0)
                    frame = 4;
                else if ((float)((float)HP / MaxHP) < 0.33f)
                    frame = 3;
                else if ((float)(HP / MaxHP) < 0.66f)
                    frame = 2;
                else
                    frame = 1;
            }
            sprite.SetFrame(frame);
        }

        public void Break(GameManager game)
        {
            sprite.SetFrame(4);
            sprite.LayerDepth = 0.1f;
            Breakable = false;
            Solid = false;
            game.PlayCue("Rock_Break");
        }

        internal void Move(Vector2 direction)
        {
            sprite.Position += direction;
            explosionSprite.Position += direction;
        }

        internal void PlayHitSFX(GameManager game)
        {
            if (Rank == 0)
                game.PlayCue("Hit_RockGray");
            else if (Rank == 1)
                game.PlayCue("Hit_RockMossy");
            else if (Rank == 2)
                game.PlayCue("Hit_RockDirt");
            else if (Rank == 3)
                game.PlayCue("Hit_RockMineral");
            else if (Rank == 4)
                game.PlayCue("Hit_RockLava");
            else if (Rank == 5)
                game.PlayCue("Hit_RockObsidian");
        }

        internal bool CanBreak(int rank)
        {
            return (rank >= Rank);
        }

        public Vector2 Center
        {
            get { return new Vector2(sprite.DrawRectangle.Center.X, sprite.DrawRectangle.Center.Y); }
        }
    }
}
