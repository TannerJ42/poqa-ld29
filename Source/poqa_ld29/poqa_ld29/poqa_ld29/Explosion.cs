﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Explosion
    {
        public bool Active = true;

        GameManager game;
        Sprite sprite;

        Animation animation;

        Vector2 center;

        float blockKillDistance = 200;
        float creatureKillDistance = 200;

        public Explosion(GameManager game, Vector2 position)
        {
            this.game = game;

            Console.WriteLine("Creating explosion at " + position.ToString());

            sprite = new Sprite(game.Content.Load<Texture2D>("Tiles/Explosion"), position);

            sprite.SetNumberOfFrames(4);

            sprite.LayerDepth = 1f;

            center = sprite.GetCenter;

            game.PlayCue("Bomb_Explode");

            animation = new Animation(new int[4] { 1, 2, 3, 4 }, 0.05f, true);
        }

        void DealDamage()
        {
            Console.WriteLine("Dealing damage.");

            foreach (Block b in game.CurrentLevel.Blocks)
            {
                if (b.Breakable &&
                    Vector2.Distance(center, b.Center) < blockKillDistance)
                {
                    b.TakeDamage(game, game.creature, 6, 100);
                }

            }

            if (Vector2.Distance(game.creature.sprite.GetCenter, center) < creatureKillDistance)
            {
                game.creature.Die();
            }
            else
            {

            }
        }


        internal void Update(GameTime gameTime)
        {
            animation.Update(gameTime);

            sprite.Width = (int)(sprite.Width * 1.3f);
            sprite.Height = (int)(sprite.Width * 1.3f);
            //sprite.Height = (int)(sprite.Height * 1.1f);

            sprite.Position.X = center.X - sprite.Width / 2;
            sprite.Position.Y = center.Y - sprite.Height / 2;

            if (sprite.Width / 2 >= creatureKillDistance)
            {
                DealDamage();
                Active = false;
            }

            sprite.SetFrame(animation.frames[animation.CurrentFrame]);
        }

        internal void Draw(SpriteBatch spriteBatch)
        {
            sprite.DrawExplosion(spriteBatch);
        }
    }
}
