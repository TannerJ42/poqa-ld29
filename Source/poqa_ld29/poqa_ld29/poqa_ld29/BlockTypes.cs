﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace poqa_ld29
{
    public enum BlockTypes
    {
        None,
        GrayBackground,
        GreenBackground,
        YellowBackground,
        OrangeBackground,
        RedBackground,
        BlackBackground,
        Entrance,
        Exit,
        Blocker,
        FakeBlocker,
        BlackBlock,
        RedBlock,
        OrangeBlock,
        YellowBlock,
        GreenBlock,
        GrayBlock,
        RedGem,
        OrangeGem,
        YellowGem,
        GreenGem,
        BlackGem,
        BlueGem,
        PurpleGem,
        WhiteGem,
        DiamondGem,
        ExplosiveBlock,
    }
}
