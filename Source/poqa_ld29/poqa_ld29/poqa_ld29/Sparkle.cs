﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Sparkle
    {
        public Sprite sprite;
        float timer;
        float shortPeriod = 0.2f;
        int Period;
        bool Active = false;

        public Sparkle(GameManager game, Vector2 position)
        {
            sprite = new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Sparkle"), position);
            sprite.SetNumberOfFrames(2);

            Period = game.Random.Next(5, 10);
            timer = Period;
        }

        public void Update(GameTime gameTime)
        {
            timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (Active)
            {
                if (timer <= 0)
                {
                    sprite.SourceRectangle.X += sprite.Width / 2;
                    if (sprite.SourceRectangle.X >= sprite.Width) // sparkle is over
                    {
                        timer = Period;
                        Active = false;
                    }
                    else
                    {
                        timer = shortPeriod;
                    }
                }
            }
            else
            {
                if (timer <= 0) // start sparkle
                {
                    Active = true;
                    timer = shortPeriod;
                    sprite.SourceRectangle.X = 0;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Active)
                sprite.Draw(spriteBatch);
        }
    }
}
