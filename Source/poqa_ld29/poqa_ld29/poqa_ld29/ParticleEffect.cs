﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Particle
    {
        public bool Active = true;
        float lifespan;
        Sprite sprite;
        float speed = 5;
        float rotationDirection;
        Vector2 direction;

        public Particle(GameManager game, Vector2 position, Vector2 direction)
        {
            
            sprite = new Sprite(game.Content.Load<Texture2D>("particle"), position);
            lifespan = game.Random.Next(1, 5);
            lifespan *= 0.1f;

            rotationDirection = game.Random.Next(0, 10);
            rotationDirection -= 5;
            rotationDirection *= 0.1f;

            float angle = game.Random.Next(0, 300);
            angle -= 150;
            angle *= 0.01f;

            direction.X += angle;
            direction.Y += angle;

            sprite.LayerDepth = 1.0f;

            speed = game.Random.Next(0, 10);
            speed *= 0.1f;

            this.direction = direction;
        }

        public void Update(GameTime gameTime)
        {
            lifespan -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (lifespan <= 0)
                Active = false;

            sprite.Position += direction * speed;

            sprite.Rotation += rotationDirection;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);
        }
    }
}
