using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace poqa_ld29
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        
        // class to hold references to useful objects
        GameManager gameManager;
        //Level testLevel;
        //Creature creature;
        //Player player;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;

            graphics.PreferredBackBufferWidth = CONSTANTS.SCREEN_WIDTH;
            graphics.PreferredBackBufferHeight = CONSTANTS.SCREEN_HEIGHT;
            graphics.IsFullScreen = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            gameManager = new GameManager(GraphicsDevice, Content);
            
            //player = new Player();
            

            //creature = new Creature(gameManager, player);
            
            //gameManager.creature = creature;
            //gameManager.player = player;

            //gameManager.LoadZone(@"Levels\Level_1");

            //creature.SetPosition(gameManager.CurrentLevel.Entrance.sprite.Position);
            //creature.currentLevel = gameManager.CurrentLevel;

            //gameManager.EnterGameplay(@"Levels\Level_1");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            //player.Update(gameTime);
            gameManager.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SaddleBrown);

            // TODO: Add your drawing code here
            //// blocks, entities only! using sprite sorting so define layer.
            //spriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend);
            //gameManager.CurrentLevel.Draw(spriteBatch);
            //creature.Draw(spriteBatch);
            //spriteBatch.End();

            //// UI, HUD
            //spriteBatch.Begin();
            //creature.DrawUI(spriteBatch);
            //spriteBatch.End();

            gameManager.Draw(spriteBatch);
            base.Draw(gameTime);
        }
    }
}
