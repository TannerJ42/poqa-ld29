﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace poqa_ld29
{
    class Creature
    {
        public bool Alive = true;

        Texture2D pixel;

        GameManager game;

        public Sprite sprite;
        public Sprite berserkSprite;

        float speed;
        float normalSpeed = 5f;
        float berserkSpeed = 8f;
        Player player;

        Direction direction;

        List<Sprite> uiSprites;

        int damage = 4;
        int rank = 0;

        SpriteFont font;
        SpriteFont smallFont;

        TextSprite levelInfo;

        TextSprite highScoreText;
        TextSprite scoreText;
        TextSprite timerText;
        TextSprite gemCount1Text;
        TextSprite gemCount2Text;
        TextSprite gemCount3Text;
        TextSprite gemCount4Text;

        Sprite checkBoxRank1;
        Sprite checkBoxRank2;
        Sprite checkBoxRank3;
        Sprite checkBoxRank4;
        Sprite checkBoxRank5;

        Cue timerWarning;

        Animator animator;

        Facing facing = Facing.Down;

        public bool CanAttack = true;
        public bool CanLeave = false;
        public bool CanMove = true;
        public bool CanIlluminate = true;

        public bool Berserk = false;
        float berserkTimer = 0;
        float berserkTime = 6f;

        public Level currentLevel;

        public int Score = 0;
        public float Timer = 200;

        float visibilityRadius = 150f;

        float leaveTimer = 0f;
        const float leaveTime = 0.75f;
        bool Leaving = false;

        public int HighScore = 10;

        public int[] BlocksBroken = new int[6];
        public int DiamondsCollected = 0;

        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle(sprite.DrawRectangle.X + sprite.DrawRectangle.Width / 4,
                                     sprite.DrawRectangle.Y + sprite.DrawRectangle.Height / 4,
                                     sprite.Width / 2,
                                     sprite.Height / 2);
            }
        }
        public Rectangle AttackRectangle
        {
            get
            {
                return new Rectangle(sprite.DrawRectangle.X + sprite.DrawRectangle.Width / 4,
                                     sprite.DrawRectangle.Y + sprite.DrawRectangle.Height / 4,
                                     sprite.Width / 2,
                                     sprite.Height / 2);
            }
        }

        public Vector2 Center
        {
            get { return new Vector2(sprite.Position.X + sprite.Width / 2, sprite.Position.Y + sprite.Height / 2); }
        }

        public Creature(GameManager game, Player player)
        {
            this.game = game;
            this.player = player;
            sprite = new Sprite(game.Content.Load<Texture2D>("Character/RockGuy"), new Vector2());
            sprite.SetNumberOfFrames(16);
            sprite.LayerDepth = 1f;

            berserkSprite = new Sprite(game.Content.Load<Texture2D>("Character/Berserk"), new Vector2());
            berserkSprite.SetNumberOfFrames(4);
            berserkSprite.LayerDepth = 0.98f;

            sprite.Origin = new Vector2(sprite.SourceRectangle.Width / 2, sprite.SourceRectangle.Height / 2);

            animator = new Animator(this, sprite);

            sprite.Rotation = (float)(Math.PI / 2);

            pixel = game.Content.Load<Texture2D>("Tiles/transparency");

            font = game.Content.Load<SpriteFont>("scoreFont");
            smallFont = game.Content.Load<SpriteFont>("HighScoreFont");

            Console.WriteLine("Creating creature");

            int x = 920;
            scoreText = new TextSprite(font, "Score: " + Score, new Vector2(x, 55));
            timerText = new TextSprite(font, "Time Left: " + Timer, new Vector2(x, 135));
            highScoreText = new TextSprite(smallFont, "High Score: " + Score, new Vector2(x, 96));


            x = 920;
            x += 22;
            int y = 480;
            gemCount1Text = new TextSprite(smallFont, "1", new Vector2(x, y));
            gemCount2Text = new TextSprite(smallFont, "1", new Vector2(x + 55 * 1, y));
            gemCount3Text = new TextSprite(smallFont, "1", new Vector2(x + 55 * 2, y));
            gemCount4Text = new TextSprite(smallFont, "1", new Vector2(x + 55 * 3, y));

            speed = normalSpeed;

            x = 920;
            int xOffset = 55;
            y = 430;
            uiSprites = new List<Sprite>()
                {
                    new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Sapphire_Blue"), new Vector2(x, y)),
                    new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Amethyst_Purple"), new Vector2(x + xOffset * 1, y)),
                    new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Opal_White"), new Vector2(x + xOffset * 2, y)),
                    new Sprite(game.Content.Load<Texture2D>("Gems/Gem_Diamond_Clear"), new Vector2(x + xOffset * 3, y)),
                }
                ;

            x = 920;
            y = 236;
            int yOffset = 30;

            checkBoxRank1 = new Sprite(game.Content.Load<Texture2D>("UI/CheckBox"), new Vector2(x, y + yOffset * 0));
            checkBoxRank2 = new Sprite(game.Content.Load<Texture2D>("UI/CheckBox"), new Vector2(x, y + yOffset * 1));
            checkBoxRank3 = new Sprite(game.Content.Load<Texture2D>("UI/CheckBox"), new Vector2(x, y + yOffset * 2));
            checkBoxRank4 = new Sprite(game.Content.Load<Texture2D>("UI/CheckBox"), new Vector2(x, y + yOffset * 3));
            checkBoxRank5 = new Sprite(game.Content.Load<Texture2D>("UI/CheckBox"), new Vector2(x, y + yOffset * 4));

            checkBoxRank1.Visible = false;
            checkBoxRank2.Visible = false;
            checkBoxRank3.Visible = false;
            checkBoxRank4.Visible = false;
            checkBoxRank5.Visible = false;


            checkBoxRank5.Position.Y += 1;

            uiSprites.Add(checkBoxRank1);
            uiSprites.Add(checkBoxRank2);
            uiSprites.Add(checkBoxRank3);
            uiSprites.Add(checkBoxRank4);
            uiSprites.Add(checkBoxRank5);

            levelInfo = new TextSprite(smallFont, "", new Vector2(95, 85));
        }

        public void SetPosition(Vector2 position)
        {
            sprite.Position = position;
        }

        public void Update(GameTime gameTime)
        {
            Timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (Timer <= 0)
            {
                Timer = 0;
                Die();
            }
            if (Timer <= 10 &&
                timerWarning == null)
            {
                timerWarning = game.audioManager.GetCue("Time_Out");
                timerWarning.Play();
            }
            if (Timer > 10 &&
                timerWarning != null)
            {
                if (timerWarning.IsPlaying)
                    timerWarning.Stop(AudioStopOptions.AsAuthored);
                timerWarning = null;
            }
            if (Leaving)
            {
                leaveTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            if (Berserk)
            {
                berserkTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                if (berserkTimer <= 0)
                {
                    EndBerserk();
                }
            }

            if (!CanLeave &&
                Vector2.Distance(sprite.Position, currentLevel.Entrance.sprite.Position) > 60 &&
                Vector2.Distance(sprite.Position, currentLevel.Exit.sprite.Position) > 60)
            {
                CanLeave = true;
            }

            if (CanMove)
            {
                Vector2 MoveDirection = new Vector2();

                switch (direction) // if direction is being held, but has been released, set to none.
                {
                    case Direction.Down:
                        if (!player.IsPressingDown)
                            direction = Direction.None;
                        break;
                    case Direction.Left:
                        if (!player.IsPressingLeft)
                            direction = Direction.None;
                        break;
                    case Direction.Right:
                        if (!player.IsPressingRight)
                            direction = Direction.None;
                        break;
                    case Direction.Up:
                        if (!player.IsPressingUp)
                            direction = Direction.None;
                        break;
                }

                CheckForNewKey();

                if (direction == Direction.None)
                {
                    CheckForOldKey();
                }

                switch (direction)
                {
                    case Direction.Up:
                        MoveDirection.Y = -1;
                        break;
                    case Direction.Down:
                        MoveDirection.Y = 1;
                        break;
                    case Direction.Right:
                        MoveDirection.X = 1;
                        break;
                    case Direction.Left:
                        MoveDirection.X = -1;
                        break;
                }

                Move(MoveDirection);
            }
            if (CanAttack)
            {
                if (player.IsPressingAction1 || Berserk)
                    animator.Attacking = true;
                if (player.IsPressingAction2)
                    animator.Dying = true;
                //if (player.IsPressingAction3)
                //{
                //    foreach (Block b in currentLevel.Blocks)
                //        b.Solid = false;
                //    sprite.Position = currentLevel.Exit.sprite.Position;
                //    CanLeave = true;
                //    leaveTimer = 1f;
                //}
            }

            for (int i = currentLevel.Pickups.Count - 1; i >= 0; i--)
            {
                if (Rectangle.Intersects(currentLevel.Pickups[i].sprite.DrawRectangle))
                {
                    currentLevel.Pickups[i].Collect(this);
                }
            }

            if (CanIlluminate)
            {
                foreach (Tile t in currentLevel.Tiles)
                {
                    if (Vector2.Distance(Center, t.Center) <= visibilityRadius)
                    {
                        t.Uncover();
                    }
                }
            }

            if (Score > HighScore)
            {
                HighScore = Score;
            }

            animator.Update(gameTime);

            scoreText.Text = "Score: " + Score;
            timerText.Text = "Time Left: " + (int)Timer;
            highScoreText.Text = "High Score: " + HighScore;

            int[] gemCounts = game.CurrentLevel.GetGemCount();

            gemCount1Text.Text = gemCounts[0].ToString();
            gemCount2Text.Text = gemCounts[1].ToString();
            gemCount3Text.Text = gemCounts[2].ToString();
            gemCount4Text.Text = gemCounts[3].ToString();

            levelInfo.Text = "Current Floor: " + (game.CurrentZone.CurrentLevel + 1).ToString() + "/5";

        }

        private void CheckForOldKey()
        {
            if (player.IsPressingDown)
            {
                direction = Direction.Down;
            }
            if (player.IsPressingUp)
            {
                direction = Direction.Up;
            }
            if (player.IsPressingRight)
            {
                direction = Direction.Right;
            }
            if (player.IsPressingLeft)
            {
                direction = Direction.Left;
            }
        }

        private void CheckForNewKey()
        {
            if (player.PressedDown)
            {
                direction = Direction.Down;
            }
            if (player.PressedUp)
            {
                direction = Direction.Up;
            }
            if (player.PressedRight)
            {
                direction = Direction.Right;
            }
            if (player.PressedLeft)
            {
                direction = Direction.Left;
            }
        }

        public void Die()
        {
            Berserk = false;
            CanAttack = false;
            CanMove = false;
            animator.Dying = true;
            berserkSprite.Visible = false;

            StartDying();

            if (timerWarning != null &&
                timerWarning.IsPlaying)
                timerWarning.Stop(AudioStopOptions.AsAuthored);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            sprite.Draw(spriteBatch);

            if (Berserk)
            {
                berserkSprite.Position = sprite.Position;
                berserkSprite.Draw(spriteBatch);
            }
        }

        public void DrawUI(SpriteBatch spriteBatch)
        {
            scoreText.Draw(spriteBatch);
            timerText.Draw(spriteBatch);
            highScoreText.Draw(spriteBatch);

            gemCount1Text.Draw(spriteBatch);
            gemCount2Text.Draw(spriteBatch);
            gemCount3Text.Draw(spriteBatch);
            gemCount4Text.Draw(spriteBatch);

            levelInfo.Draw(spriteBatch);

            foreach (Sprite s in uiSprites)
                s.Draw(spriteBatch);
        }

        void Move(Vector2 direction)
        {
            List<Rectangle> collisions = new List<Rectangle>();

            if (direction.X != 0 ||
                direction.Y != 0)
                animator.Velocity = 1;
            else
                animator.Velocity = 0;

            sprite.Position.X += direction.X * speed;
            sprite.Position.Y += direction.Y * speed;

            bool collide = true;

            int tries = 25;

            while (collide && tries > 0)
            {
                tries--;
                collide = false;
                foreach (Block b in currentLevel.Blocks)
                {
                    if (b.Solid &&
                        b.HitRectangle.Intersects(Rectangle))
                    {
                        collide = true;
                        if (direction.X > 0 && // right
                            Rectangle.Right > b.HitRectangle.Left)
                        {
                            sprite.Position.X -= Rectangle.Right - b.HitRectangle.Left;
                        }

                        if (direction.X < 0 && // left
                            Rectangle.Left < b.HitRectangle.Right)
                        {
                            sprite.Position.X += b.HitRectangle.Right - Rectangle.Left;
                        }

                        if (direction.Y < 0 && // up
                            Rectangle.Top < b.HitRectangle.Bottom)
                        {
                            sprite.Position.Y -= Rectangle.Top - b.HitRectangle.Bottom;
                        }

                        if (direction.Y > 0 && // down
                            Rectangle.Bottom > b.HitRectangle.Top)
                        {
                            sprite.Position.Y += b.HitRectangle.Top - Rectangle.Bottom;
                        }
                    }
                }
            }


            if (direction.X < 0) // left
            {
                facing = Facing.Left;
                sprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                sprite.Rotation = 0;
                berserkSprite.SpriteEffect = SpriteEffects.FlipHorizontally;
                berserkSprite.Rotation = 0;
            }
            else if (direction.X > 0) // right
            {
                facing = Facing.Right;
                sprite.SpriteEffect = SpriteEffects.None;
                sprite.Rotation = 0;
                berserkSprite.SpriteEffect = SpriteEffects.None;
                berserkSprite.Rotation = 0;
            }
            else if (direction.Y > 0) // down
            {
                facing = Facing.Down;
                sprite.SpriteEffect = SpriteEffects.None;
                sprite.Rotation = (float)(Math.PI / 2);
                berserkSprite.SpriteEffect = SpriteEffects.None;
                berserkSprite.Rotation = (float)(Math.PI / 2);
            }
            else if (direction.Y < 0) // up
            {
                facing = Facing.Up;
                sprite.SpriteEffect = SpriteEffects.None;
                sprite.Rotation = -(float)(Math.PI / 2);
                berserkSprite.SpriteEffect = SpriteEffects.None;
                berserkSprite.Rotation = -(float)(Math.PI / 2);
            }

            if (CanLeave)
            {
                Doors door = Doors.None;

                if (currentLevel.Exit.Rectangle.Intersects(sprite.DrawRectangle))
                {
                    door = Doors.Exit;
                }

                else if (currentLevel.Entrance.Rectangle.Intersects(sprite.DrawRectangle) &&
                    currentLevel.Entrance.Open)
                {
                    door = Doors.Entrance;
                }
                else
                {
                    Leaving = false;
                }

                if (door != Doors.None)
                {
                    if (!Leaving)
                    {
                        Leaving = true;
                        leaveTimer = leaveTime;
                    }
                    else
                    {
                        if (leaveTimer <= 0)
                        {
                            currentLevel.LeaveLevel(door);
                        }
                    }
                }
            }
        }

        void AttackBlocks()
        {
            Rectangle attackRectangle = Rectangle;

            switch (facing)
            {
                case Facing.Down:
                    attackRectangle.Y += attackRectangle.Height / 2;
                    break;
                case Facing.Up:
                    attackRectangle.Y -= attackRectangle.Height / 2;
                    break;
                case Facing.Left:
                    attackRectangle.X -= attackRectangle.Width / 2;
                    break;
                case Facing.Right:
                    attackRectangle.X += attackRectangle.Width / 2;
                    break;
            }

            int multiplier = 1;

            if (Berserk)
                multiplier++;

            foreach (Block b in currentLevel.Blocks)
            {
                if (b.HitRectangle.Intersects(attackRectangle))
                {
                    b.TakeDamage(game, this, rank, damage * multiplier);
                }
            }
        }

        internal void StartAttacking()
        {
            if (!Berserk)
                CanMove = false;

            CanAttack = false;

            Rectangle attackRectangle = Rectangle;

            switch (facing)
            {
                case Facing.Down:
                    attackRectangle.Y += attackRectangle.Height / 2;
                    break;
                case Facing.Up:
                    attackRectangle.Y -= attackRectangle.Height / 2;
                    break;
                case Facing.Left:
                    attackRectangle.X -= attackRectangle.Width / 2;
                    break;
                case Facing.Right:
                    attackRectangle.X += attackRectangle.Width / 2;
                    break;
            }

            bool hit = false;
            foreach (Block b in currentLevel.Blocks)
            {
                if (b.HitRectangle.Intersects(attackRectangle) &&
                    b.Solid)
                {
                    hit = true;

                    if (!b.Breakable || !b.CanBreak(rank))
                        game.PlayCue("Hit_RockFail");
                    else
                    {
                        CreateParticles(b.Rank);
                        b.PlayHitSFX(game);
                    }
                }
            }

            if (!hit)
            {
                game.PlayCue("Hit_None");
            }
        }

        void CreateParticles(int rank)
        {
            Vector2 left = sprite.GetCenter;
            Vector2 right = sprite.GetCenter;

            switch (direction)
            {
                case Direction.Left:
                    break;
                case Direction.Right:
                    break;
                case Direction.Up:
                    break;
                case Direction.Down:
                    break;
            }
            game.CurrentLevel.AddParticles(left);
            game.CurrentLevel.AddParticles(right);
                        
        }

        internal void StopAttacking()
        {
            CanMove = true;
            CanAttack = true;

            AttackBlocks();
        }

        enum Facing
        {
            Down,
            Up,
            Left,
            Right
        }

        internal void PickUpGem(Gem gem)
        {
            int newRank = 0;
            int newDamage = 0;

            switch (gem.type)
            {
                case Gems.green:
                    newDamage = 5;
                    newRank = 1;
                    break;
                case Gems.yellow:
                    newDamage = 7;
                    newRank = 2;
                    break;
                case Gems.orange:
                    newDamage = 9;
                    newRank = 3;
                    break;
                case Gems.red:
                    newDamage = 12;
                    newRank = 4;
                    break;
                case Gems.black:
                    newDamage = 15;
                    newRank = 5;
                    break;
                case Gems.blue:
                    Timer += 20;
                    break;
                case Gems.purple:
                    Timer += 40;
                    break;
                case Gems.white:
                    Timer += 60;
                    break;
                case Gems.diamond:
                    Score += 1000;
                    DiamondsCollected += 1;
                    StartBerserk();
                    break;
            }

            if (newRank > rank)
                rank = newRank;
            if (newDamage > damage)
                damage = newDamage;

            ShowChecks();

            game.PlayCue("Gem_Pickup");
        }

        void ShowChecks()
        {
            if (rank >= 1)
                checkBoxRank1.Visible = true;
            if (rank >= 2)
                checkBoxRank2.Visible = true;
            if (rank >= 3)
                checkBoxRank3.Visible = true;
            if (rank >= 4)
                checkBoxRank4.Visible = true;
            if (rank >= 5)
                checkBoxRank5.Visible = true;
        }

        internal void StartBerserk()
        {
            Berserk = true;
            speed = berserkSpeed;
            berserkTimer = berserkTime;
            CanMove = true;

            game.StartBerserkAudio();
        }

        internal void EndBerserk()
        {
            Berserk = false;
            speed = normalSpeed;

            game.StopBerserkAudio();
        }

        internal void StartDying()
        {
            if (Alive)
            {
                CanMove = false;
                CanAttack = false;
                Alive = false;

                game.PlayBGM("Defeat_Music");
            }
        }

        internal void StopDying()
        {
            CanIlluminate = false;
            game.StartLosing();
        }

        internal void BrokeBlock(int Rank)
        {
            Score += 50 * (Rank + 1);

            BlocksBroken[Rank] += 1;

        }

        internal void AddTimerToScore()
        {
            Console.WriteLine("Alive: " + Alive);

            if (Alive)
            {
                Score += (int)(10 * (Timer));
            }
        }

        enum Direction
        {
            None,
            Up,
            Down,
            Left,
            Right
        }
    }
}
