﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework.Storage;

namespace poqa_ld29
{
    class SaveManager
    {
        XmlSerializer serializer;
        StorageDevice device;
        IAsyncResult result;

        public bool HasDevice
        {
            get { return (device != null); }
        }

        public SaveManager()
        {
            //serializer = new XmlSerializer(typeof(SaveData));
            serializer = XmlSerializer.FromTypes(new[] { typeof(SaveData) })[0];
        }

        public void Update()
        {
            if (!HasDevice)
            {
                if (result == null)
                    result = StorageDevice.BeginShowSelector(null, null);
                else if (result.IsCompleted)
                    device = StorageDevice.EndShowSelector(result);
            }
        }

        public SaveData CreateOrLoadSave()
        {
            if (SaveExists())
            {
                return LoadSave();
            }
            else
            {
                SaveData data = new SaveData();

                data.score0 = 5000;
                data.score1 = 10000;
                data.score2 = 20000;
                data.score3 = 40000;

                return data;
            }
        }

        private bool SaveExists()
        {
            IAsyncResult result = device.BeginOpenContainer("Scores", null, null);
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            bool exists = container.FileExists("save.dat");

            container.Dispose();

            return exists;
        }

        public SaveData LoadSave()
        {
            Console.WriteLine("Loading!");

            SaveData data;

            IAsyncResult result = device.BeginOpenContainer("Scores", null, null);
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            Stream stream = container.OpenFile("save.dat", FileMode.Open);

            data = (SaveData)serializer.Deserialize(stream);

            stream.Close();

            container.Dispose();


            return data;
        }

        internal void SaveData(SaveData data)
        {
            Console.WriteLine("Saving!");

            //if (SaveExists())
            //    DeleteSave();

            IAsyncResult result = device.BeginOpenContainer("Scores", null, null);
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            using (container.CreateFile("save.dat"))
            {
                
            }

            Stream stream = container.OpenFile("save.dat", FileMode.Open);

            serializer.Serialize(stream, data);

            stream.Close();

            container.Dispose();
        }

        private void DeleteSave()
        {
            IAsyncResult result = device.BeginOpenContainer("Scores", null, null);
            result.AsyncWaitHandle.WaitOne();

            StorageContainer container = device.EndOpenContainer(result);

            container.DeleteFile("save.dat");

            container.Dispose();
        }
    }

    public class SaveData
    {
        public int score0;
        public int score1;
        public int score2;
        public int score3;
    }
}
