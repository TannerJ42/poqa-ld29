﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Audio;

namespace poqa_ld29
{
    /// <summary>
    /// Interacts with XACT.
    /// </summary>
    class AudioManager
    {
        AudioEngine audioEngine;
        WaveBank waveBank;
        SoundBank soundBank;

        Cue bgm;
        Cue berserkBGM;

        public AudioManager()
        {
            LoadAudioContent();
        }

        public void LoadAudioContent()
        {
            audioEngine = new AudioEngine(@"Content/LD29audio.xgs");
            waveBank = new WaveBank(audioEngine, @"Content/Wave Bank.xwb");
            soundBank = new SoundBank(audioEngine, @"Content/Sound Bank.xsb");
        }

        internal void PlayCue(string name)
        {
            Cue cue = soundBank.GetCue(name);
            cue.Play();
        }

        public void PlayBGM(string name)
        {
            if (bgm != null &&
                bgm.IsPlaying)
                StopBGM();
            bgm = soundBank.GetCue(name);
            bgm.Play();
        }

        public void StopBGM()
        {
            bgm.Stop(AudioStopOptions.Immediate);

            if (berserkBGM != null &&
                berserkBGM.IsPlaying)
                berserkBGM.Stop(AudioStopOptions.Immediate);
        }

        public void StartBerserk()
        {
            if (bgm != null)
                bgm.Pause();

            if (berserkBGM != null &&
                berserkBGM.IsPlaying)
                berserkBGM.Stop(AudioStopOptions.Immediate);

            berserkBGM = soundBank.GetCue("Berserk_Music");
            berserkBGM.Play();
        }

        public Cue GetCue(string name)
        {
            return soundBank.GetCue(name);
        }

        public void StopBerserk()
        {
            bgm.Resume();
            berserkBGM.Stop(AudioStopOptions.Immediate);
        }
    }
}
