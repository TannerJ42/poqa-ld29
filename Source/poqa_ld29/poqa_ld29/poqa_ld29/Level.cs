﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace poqa_ld29
{
    class Level
    {
        Texture2D background;
        public List<Block> Blocks;
        public List<Pickup> Pickups;
        public List<Tile> Tiles;
        public List<Explosion> Explosions;
        List<Particle> Particles;

        GameManager game;

        MouseState lastRoundState;

        //public Vector2 EntrancePosition;
        //public Vector2 ExitPosition;

        public Door Entrance;
        public Door Exit;

        /// <summary>
        /// Holds all info needed for a Player to play a level.
        /// </summary>
        public Level(GameManager game, Zone zone)
        {
            this.game = game;
            Blocks = new List<Block>();
            Pickups = new List<Pickup>();
            Tiles = new List<Tile>();
            Explosions = new List<Explosion>();
            Particles = new List<Particle>();

            Console.WriteLine("Creating level.");
        }

        public void SetBackground(Texture2D background)
        {
            this.background = background;
        }

        public void AddBlock(Block block)
        {
            Blocks.Add(block);
        }

        public void ParseDrop(BlockTypes blockType, Vector2 position)
        {
            position *= CONSTANTS.BLOCK_SIZE;

            switch (blockType)
            {
                case BlockTypes.Entrance:
                    Entrance = new Door(game, Doors.Entrance, position);
                    break;
                case BlockTypes.Exit:
                    Exit = new Door(game, Doors.Exit, position);
                    break;
                default:
                    Pickups.Add(new Gem(game, blockType, position));
                    break;
            }
        }

        public void Update(GameTime gameTime)
        {
            //MouseState state = Mouse.GetState();

            //for (int i = Blocks.Count - 1; i >= 0; i--) // debug mouse clicks
            //{
            //    Blocks[i].Update(gameTime);
            //    if (state.LeftButton == ButtonState.Pressed &&
            //        lastRoundState.LeftButton == ButtonState.Released)
            //    {
            //        if (Blocks[i].HitRectangle.Contains(new Point(state.X, state.Y)) && Blocks[i].Breakable)
            //        {
            //            Blocks[i].TakeDamage(6, 99);
            //        }
            //    }
            //}

            //lastRoundState = state;

            // update sparkles
            foreach (Block b in Blocks)
                b.Update(gameTime);

            for (int i = Particles.Count - 1; i >= 0; i--)
            {
                Particles[i].Update(gameTime);
                if (!Particles[i].Active)
                    Particles.RemoveAt(i);
            }

            for (int i = Explosions.Count - 1; i >= 0; i--)
            {
                Explosions[i].Update(gameTime);
                if (!Explosions[i].Active)
                    Explosions.RemoveAt(i);
            }

            game.CurrentLevel.Exit.CheckForSparkle(game.CurrentLevel.Blocks);
            game.CurrentLevel.Entrance.CheckForSparkle(game.CurrentLevel.Blocks);
            for (int i = game.CurrentLevel.Pickups.Count - 1; i >= 0; i--)
            {
                game.CurrentLevel.Pickups[i].CheckForSparkle(game.CurrentLevel.Blocks);
                if (!game.CurrentLevel.Pickups[i].Active)
                {
                    Console.WriteLine("Removing!");
                    game.CurrentLevel.Pickups.RemoveAt(i);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = Tiles.Count - 1; i >= 0; i--)
            {
                Tiles[i].Draw(spriteBatch);
            }

            for (int i = Pickups.Count - 1; i >= 0; i--)
            {
                Pickups[i].Draw(spriteBatch);
            }

            for (int i = Blocks.Count - 1; i >= 0; i--)
            {
                Blocks[i].Draw(spriteBatch);
            }

            foreach (Explosion e in Explosions)
                e.Draw(spriteBatch);

            foreach (Particle p in Particles)
                p.Draw(spriteBatch);

            Entrance.Draw(spriteBatch);
            Exit.Draw(spriteBatch);
        }

        public void DrawUI(SpriteBatch spriteBatch)
        {
            foreach (Gem g in Pickups)
                g.DrawUI(spriteBatch);
        }

        internal void AddBackground(Tile tile)
        {
            Tiles.Add(tile);
        }

        internal void Move(Vector2 direction)
        {
            foreach (Tile t in Tiles)
                t.Move(direction);
            foreach (Block b in Blocks)
                b.Move(direction);
            foreach (Pickup p in Pickups)
                p.Move(direction);

            Entrance.Move(direction);
            Exit.Move(direction);
        }

        public void LeaveLevel(Doors door)
        {
            game.Leave(door);
        }

        internal void DisableEntrance()
        {
            Entrance.sprite.Visible = false;
            Entrance.Open = false;
        }

        internal int[] GetGemCount()
        {
            int[] gemCount = new int[4]
                {
                    0,
                    0,
                    0,
                    0
                };

            foreach (Gem g in Pickups)
            {
                if (g.type == Gems.blue)
                    gemCount[0]++;
                if (g.type == Gems.purple)
                    gemCount[1]++;
                if (g.type == Gems.white)
                    gemCount[2]++;
                if (g.type == Gems.diamond)
                    gemCount[3]++;
            }

            return gemCount;
        }

        internal void AddExplosion(Vector2 vector2)
        {
            Explosions.Add(new Explosion(game, vector2));
        }

        public void AddParticles(Vector2 location)
        {
            for (int i = game.Random.Next(3, 10); i >= 0; i--)
                Particles.Add(new Particle(game, location, new Vector2(1, 0)));
        }
    }
}
