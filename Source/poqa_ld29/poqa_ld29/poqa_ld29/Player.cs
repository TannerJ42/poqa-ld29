﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace poqa_ld29
{
    /// <summary>
    /// Holds mouse, keyboard, player state info
    /// </summary>
    class Player
    {
        KeyboardState lastRoundKeyboardState;
        KeyboardState currentRoundKeyboardState;

        public Player()
        {
            currentRoundKeyboardState = Keyboard.GetState();
        }

        public void Update(GameTime gameTime)
        {
            lastRoundKeyboardState = currentRoundKeyboardState;
            currentRoundKeyboardState = Keyboard.GetState();
        }

        public void Draw(SpriteBatch spriteBatch)
        {

        }

        public bool IsPressingRight
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.Right) ||
                        currentRoundKeyboardState.IsKeyDown(Keys.D));
            }
        }

        public bool IsPressingLeft
        {
            get { return (currentRoundKeyboardState.IsKeyDown(Keys.Left) ||
                          currentRoundKeyboardState.IsKeyDown(Keys.A)); }
        }

        public bool IsPressingUp
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.Up) ||
                        currentRoundKeyboardState.IsKeyDown(Keys.W));
            }
        }

        public bool IsPressingDown
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.Down) ||
                        currentRoundKeyboardState.IsKeyDown(Keys.S));
            }
        }

        public bool PressedDown
        {
            get
            {
                return (PressedDownKey(Keys.Down) ||
                        PressedDownKey(Keys.S));
            }
        }

        public bool PressedUp
        {
            get
            {
                return (PressedDownKey(Keys.Up) ||
                        PressedDownKey(Keys.W));
            }
        }

        public bool PressedLeft
        {
            get
            {
                return (PressedDownKey(Keys.Left) ||
                        PressedDownKey(Keys.A));
            }
        }

        public bool PressedRight
        {
            get
            {
                return (PressedDownKey(Keys.Right) ||
                        PressedDownKey(Keys.D));
            }
        }

        public bool IsPressingAction1
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.Enter) ||
                        currentRoundKeyboardState.IsKeyDown(Keys.Space));
            }
        }

        public bool PressedAction1
        {
            get
            {
                return (PressedUpKey(Keys.Space) ||
                        PressedUpKey(Keys.Enter));
            }
        }

        public bool IsPressingAction2
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.Escape));
            }
        }

        public bool IsPressingAction3
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.RightControl));
            }
        }

        public bool PressedAction2
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyUp(Keys.Escape) &&
                        lastRoundKeyboardState.IsKeyDown(Keys.Escape));
            }
        }

        bool PressingKey(Keys key)
        {
            return (currentRoundKeyboardState.IsKeyDown(key));
        }

        bool PressedUpKey(Keys key)
        {
            return (currentRoundKeyboardState.IsKeyUp(key) &&
                    lastRoundKeyboardState.IsKeyDown(key));
        }

        bool PressedDownKey(Keys key)
        {
            return (currentRoundKeyboardState.IsKeyDown(key) &&
                    lastRoundKeyboardState.IsKeyUp(key));
        }

        public bool PressedAction3
        {
            get
            {
                return (currentRoundKeyboardState.IsKeyDown(Keys.RightControl));
            }
        }
    }
}
