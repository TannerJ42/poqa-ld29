﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class LevelLoader
    {
        GameManager game;
        ContentManager content;

        Dictionary<Color, BlockTypes> blockTypeDict;

        /// <summary>
        /// Class to handle level generation.
        /// </summary>
        public LevelLoader(GameManager game, ContentManager content)
        {
            this.game = game;
            this.content = content;

            blockTypeDict = new Dictionary<Color, BlockTypes>
            {
                {new Color(255, 100, 255), BlockTypes.Entrance}, // pink
                {new Color(0, 150, 255), BlockTypes.Exit}, // sky blue
                {new Color(5, 5, 5), BlockTypes.Blocker}, // blackish
                {new Color(255, 0, 0), BlockTypes.RedBlock}, // red
                {new Color(255, 150, 0), BlockTypes.OrangeBlock}, // orange
                {new Color(255, 255, 0), BlockTypes.YellowBlock}, // yellow
                {new Color(0, 255, 0), BlockTypes.GreenBlock}, // green
                {new Color(125, 125, 125), BlockTypes.BlackBlock}, // black
                {new Color(150, 150, 150), BlockTypes.GrayBlock}, // gray
                {new Color(150, 0, 0), BlockTypes.RedGem}, //red
                {new Color(150, 75, 0), BlockTypes.OrangeGem}, //orange
                {new Color(150, 150, 0), BlockTypes.YellowGem}, //yellow
                {new Color(0, 150, 0), BlockTypes.GreenGem}, // green
                {new Color(100, 100, 100), BlockTypes.BlackGem}, // black
                {new Color(0, 0, 255), BlockTypes.BlueGem}, // blue
                {new Color(254, 0, 254), BlockTypes.PurpleGem}, // purple
                {new Color(200, 200, 200), BlockTypes.WhiteGem}, // whiteish
                {new Color(100, 255, 255), BlockTypes.DiamondGem}, // Pale blue
                {new Color(25, 25, 25), BlockTypes.FakeBlocker},
                {new Color(255, 255, 255), BlockTypes.GrayBackground},
                {new Color(0, 150, 150), BlockTypes.GreenBackground},
                {new Color(75, 75, 0), BlockTypes.YellowBackground},
                {new Color(75, 0, 0), BlockTypes.OrangeBackground},
                {new Color(50, 0, 0), BlockTypes.RedBackground},
                {new Color(75, 75, 75), BlockTypes.BlackBackground},
                {new Color(150, 0, 255), BlockTypes.ExplosiveBlock},
            };
        }

        /// <summary>
        /// Takes one 3x1 level sheet and turns it into a level.
        /// </summary>
        /// <param name="levelSheet"></param>
        /// <returns></returns>
        public void LoadLevel(Zone zone, Texture2D levelSheet)
        {
            Texture2D background = SplitImage(game.GraphicsDevice, levelSheet, new Rectangle((levelSheet.Width / 3) * 2, 0, levelSheet.Width / 3, levelSheet.Height));
            Texture2D drops = SplitImage(game.GraphicsDevice, levelSheet, new Rectangle((levelSheet.Width / 3) * 0, 0, levelSheet.Width / 3, levelSheet.Height));
            Texture2D blocks = SplitImage(game.GraphicsDevice, levelSheet, new Rectangle((levelSheet.Width / 3) * 1, 0, levelSheet.Width / 3, levelSheet.Height));

            LoadLevel(zone, background, drops, blocks);
        }

        /// <summary>
        /// Turns three tilemaps into a level.
        /// </summary>
        private void LoadLevel(Zone zone, Texture2D background, Texture2D drops, Texture2D blocks)
        {
            Level level = new Level(game, zone);

            game.Seed = new Random(101010);

            // make sure all three images match in size
            bool match = true;
            match = CheckSize(background, drops) && CheckSize(drops, blocks);
            if (!match) // size mismatch
                throw new Exception("Tilemaps don't match.");

            // create background as texture2D
            CreateBackground(background, level);

            // place powerups and starting and ending position
            ParseDrops(drops, level);

            // generate blocks
            GenerateBlocks(blocks, level);

            // error checking. No blocks outside of background, etc.

            // center level around entrance.
            CenterLevel(level, Doors.Entrance);

            level.Entrance.CheckForSparkle(level.Blocks);
            level.Exit.CheckForSparkle(level.Blocks);

            foreach (Gem g in level.Pickups)
            {
                g.CheckForSparkle(level.Blocks);
            }

            zone.Levels.Add(level);
        }

        public static void CenterLevel(Level level, Doors door)
        {
            Door currentDoor;

            if (door == Doors.Entrance)
                currentDoor = level.Entrance;
            else
                currentDoor = level.Exit;

            if (currentDoor.sprite.Position.X > 640)
                level.Move(new Vector2(640 - currentDoor.sprite.Position.X, 0));
            if (currentDoor.sprite.Position.X < 400)
                level.Move(new Vector2(currentDoor.sprite.Position.X - 400, 0));
            if (currentDoor.sprite.Position.Y > 350)
                level.Move(new Vector2(0, 350 - currentDoor.sprite.Position.Y));
            if (currentDoor.sprite.Position.Y < 200)
                level.Move(new Vector2(0, currentDoor.sprite.Position.Y - 200));
        }

        private void CreateBackground(Texture2D background, Level level)
        {
            Color[] colors = GetColors(background);

            int x = 0;
            int y = 0;

            for (int i = 0; i <= colors.Length - 1; i++)
            {
                if (colors[i].A > 0)
                    level.AddBackground(new Tile(game, ColorToBlock(colors[i]), new Vector2(x, y)));
                x += 1;
                if (x > background.Width - 1)
                {
                    x = 0;
                    y += 1;
                }
            }
        }

        /// <summary>
        /// Iterate through pixels in image. Create corresponding block.
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="level"></param>
        private void GenerateBlocks(Texture2D blocks, Level level)
        {
            Color[] colors = GetColors(blocks);

            int x = 0;
            int y = 0;

            for (int i = 0; i <= colors.Length - 1; i++)
            {
                if (colors[i].A > 0)
                    level.AddBlock(new Block(game, ColorToBlock(colors[i]), new Vector2(x, y)));
                x += 1;
                if (x > blocks.Width - 1)
                {
                    x = 0;
                    y += 1;
                }
            }
        }

        /// <summary>
        /// Iterate through pixels in image. Create corresponding block.
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="level"></param>
        private void ParseDrops(Texture2D drops, Level level)
        {
            Color[] colors = GetColors(drops);

            int x = 0;
            int y = 0;

            for (int i = 0; i <= colors.Length - 1; i++)
            {

                if (colors[i].A > 0)
                    level.ParseDrop(ColorToBlock(colors[i]), new Vector2(x, y));
                x += 1;
                if (x > drops.Width - 1)
                {
                    x = 0;
                    y += 1;
                }
            }
        }

        private Texture2D SplitImage(GraphicsDevice graphics, Texture2D sourceImage, Rectangle sourceRectangle)
        {
            // create a new texture to hold the pixel data
            Texture2D newImage = new Texture2D(graphics, sourceRectangle.Width, sourceRectangle.Height);

            // Grab data from sourceImage
            Color[] data = new Color[sourceRectangle.Width * sourceRectangle.Height];
            sourceImage.GetData(0, sourceRectangle, data, 0, data.Length);
            newImage.SetData(data);

            return newImage;

        }

        private bool CheckSize(Texture2D first, Texture2D second)
        {
            if (TextureLength(first) == TextureLength(second)) // match
                return true;
            Console.WriteLine(first.Name + " and " + second.Name + " are a different length.");
            return false;
        }

        int TextureLength(Texture2D texture)
        {
            return texture.Width * texture.Height;
        }

        Color[] GetColors(Texture2D texture)
        {
            Color[] colors = new Color[texture.Width * texture.Height];

            texture.GetData<Color>(colors);

            return colors;
        }

        BlockTypes ColorToBlock(Color color)
        {
            BlockTypes blockType = BlockTypes.None;

            blockType = blockTypeDict[color];


            return blockType;
        }
    }
}
