﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Result
    {
        SpriteFont font;
        bool ShowImage = false;
        bool ShowText = true;
        Sprite image;
        TextSprite text;
        int score;

        public Result(Sprite sprite, Vector2 position, SpriteFont font, int score, string text)
        {
            int Width = 0;

            if (sprite != null)
            {
                ShowImage = true;
                image = sprite;
                Width = sprite.Width;
            }
            else
            {

            }


            this.font = font;

            this.score = score;

            text = text + " " + score.ToString() + " Points";

            this.text = new TextSprite(font, text, new Vector2(position.X + Width + 10, position.Y + 5));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (ShowImage)
            {
                image.Draw(spriteBatch);
            }

            if (ShowText)
            {
                text.Draw(spriteBatch);
            }
        }

        internal void Center()
        {
            text.Center();
        }
    }
}
