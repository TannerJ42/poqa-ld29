﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace poqa_ld29
{
    class Gem : Pickup
    {
        //public int Tier;

        public bool Covered = false;
        public Gems type;
        Sparkle sparkle;
        Block block;
        Vector2 destination;
        public bool PickedUp = false;
        float speed = 1.0f;

        public Gem(GameManager game, BlockTypes blockType, Vector2 position) : base(game, blockType, position)
        {
            string image;

            destination = new Vector2(1100, 250);

            int x = 955;

            int x2 = 925;
            int y2 = 226;
            int yOffset = 30;

            new Vector2(x2, y2 + yOffset * 0);
            new Vector2(x2, y2 + yOffset * 1);
            new Vector2(x2, y2 + yOffset * 2);
            new Vector2(x2, y2 + yOffset * 3);
            new Vector2(x2, y2 + yOffset * 4);

            switch (blockType)
            {
                case BlockTypes.GreenGem:
                    image = "Gems/Gem_Emerald_Green";
                    type = Gems.green;
                    destination = new Vector2(x2, y2 + yOffset * 0);
                    //Tier = 1;
                    break;
                case BlockTypes.YellowGem:
                    image = "Gems/Gem_Citrine_Yellow";
                    type = Gems.yellow;
                    destination = new Vector2(x2, y2 + yOffset * 1);
                    //Tier = 2;
                    break;
                case BlockTypes.OrangeGem:
                    image = "Gems/Gem_Topaz_Orange";
                    type = Gems.orange;
                    destination = new Vector2(x2, y2 + yOffset * 2);
                    //Tier = 3;
                    break;
                case BlockTypes.RedGem:
                    image = "Gems/Gem_Ruby_Red";
                    type = Gems.red;
                    destination = new Vector2(x2, y2 + yOffset * 3);
                    //Tier = 4;
                    break;
                case BlockTypes.BlackGem:
                    image = "Gems/Gem_Quartz_Rainbow";
                    destination = new Vector2(x2, y2 + yOffset * 4);
                    type = Gems.black;
                    //Tier = 5;
                    break;
                case BlockTypes.BlueGem:
                    image = "Gems/Gem_Sapphire_Blue";
                    type = Gems.blue;
                    destination = new Vector2(x, 445);
                    break;
                case BlockTypes.PurpleGem:
                    image = "Gems/Gem_Amethyst_Purple";
                    type = Gems.purple;
                    destination = new Vector2(x + 55 * 1, 445);
                    break;
                case BlockTypes.WhiteGem:
                    image = "Gems/Gem_Opal_White";
                    type = Gems.white;
                    destination = new Vector2(x + 55 * 2, 445);
                    break;
                case BlockTypes.DiamondGem:
                    image = "Gems/Gem_Diamond_Clear";
                    type = Gems.diamond;
                    destination = new Vector2(x + 55 * 3, 445);
                    break;
                default:
                    image = "testDrop";
                    break;
            }

            sprite = new Sprite(game.Content.Load<Texture2D>(image), position);
            sprite.LayerDepth = 0.3f;

            sparkle = new Sparkle(game, sprite.Position);
            sparkle.sprite.LayerDepth = 0.9f;
        }

        public override void CheckForSparkle(List<Block> blocks)
        {
            foreach (Block b in blocks)
                if (b.HitRectangle.Intersects(sprite.DrawRectangle))
                {
                    Covered = true;
                    block = b;
                }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Active)
            {
                if (!PickedUp)
                {
                    if (block != null &&
                        !block.Solid)
                    {
                        Covered = false;
                    }
                    if (Covered)
                        sparkle.Update(gameTime);
                }
                else
                {
                    if (Vector2.Distance(sprite.Position, destination) > 30)
                    {
                        speed += 0.5f;
                        Vector2 direction = destination - sprite.Position;

                        direction *= 0.01f;

                        //sprite.Rotation += 0.1f;

                        Vector2 center = sprite.GetCenter;

                        sprite.Width = (int)(0.99f * sprite.Width);
                        sprite.Height = (int)(0.99f * sprite.Height);

                        sprite.Position.X = center.X - sprite.Width / 2;
                        sprite.Position.Y = center.Y - sprite.Height / 2;

                        sprite.Position += speed * direction;

                        
                    }
                    else
                        Active = false;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (!PickedUp)
            {
                if (Covered)
                    sparkle.Draw(spriteBatch);
                base.Draw(spriteBatch);
            }
        }

        public void DrawUI(SpriteBatch spriteBatch)
        {
            if (PickedUp)
            {
                base.Draw(spriteBatch);
            }
        }

        public override void Move(Vector2 direction)
        {
            sparkle.sprite.Position += direction;
            base.Move(direction);

        }

        public override void Collect(Creature creature)
        {
            if (!this.PickedUp)
            {
                base.Collect(creature);

                creature.PickUpGem(this);
                sprite.LayerDepth = 1f;

                this.PickedUp = true;
            }
        }
    }

    public enum Gems
    {
        green,
        yellow,
        orange,
        red,
        black,
        blue,
        purple,
        white,
        diamond
    }
}
