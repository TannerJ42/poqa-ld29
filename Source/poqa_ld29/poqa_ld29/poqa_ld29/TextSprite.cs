﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace poqa_ld29
{

    class TextSprite
    {
        SpriteFont font;

        public string Text;
        public Vector2 Position;

        public Color Color = new Color(220, 221, 222);
        public bool Visible = true;

        public TextSprite(SpriteFont font, string text, Vector2 position)
        {
            this.font = font;
            this.Text = text;
            this.Position = position;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, Text, Position, Color);
        }

        /// <summary>
        /// Center the sprite to its initial Vector2
        /// </summary>
        /// <param name="horizontal">"Center horizontally"</param>
        /// <param name="vertical">"Center vertically"</param>
        public void Center()
        {
            Position.X -= font.MeasureString(Text).Length() / 2;
        }

        public void RightJustify()
        {
            Position.X -= font.MeasureString(Text).Length();
        }
    }
}
